import org.irundaia.sbt.sass._
import org.irundaia.sbt.sass.SbtSassify.autoImport.SassKeys._
import WebKeys._

val productionEnv =
  sys.env.getOrElse("JS_ENV", "").equalsIgnoreCase("production")

val jsBuildTask = Def.taskDyn {
  if (productionEnv) {
    Compile / fullOptJS
  } else {
    Compile / fastOptJS
  }
}

val cssStyle = if (productionEnv) {
  Minified
} else {
  Maxified
}

resolvers += Resolver.sonatypeRepo("releases")
Global / onChangedBuildSource := ReloadOnSourceChanges
Global / lintUnusedKeysOnLoad := false
ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.4.3"
ThisBuild / scalafixScalaBinaryVersion := "2.13"

addCompilerPlugin(
  "org.typelevel" %% "kind-projector" % "0.13.0" cross CrossVersion.full,
)
addCompilerPlugin("com.lihaoyi" %% "acyclic" % "0.2.1")
addCompilerPlugin(scalafixSemanticdb)

dependencyUpdatesFilter -= moduleFilter(
  organization = "org.scalameta",
  name = "semanticdb-scalac",
)

val circeVersion = "0.14.1"

lazy val root = (project in file("."))
  .enablePlugins(ScalaJSPlugin)
  .enablePlugins(JSDependenciesPlugin)
  .enablePlugins(BuildInfoPlugin)
  .enablePlugins(SbtWeb)
  .enablePlugins(ChromeBundlerPlugin)
  .settings(
    organization := "com.kklorenzotesta",
    scalaVersion := "2.13.6",
    name := "YoutubePreamp",
    description := "A simple preamp for soundsystem addict",
    Compile / mainClass := Some(
      "com.kklorenzotesta.youtubepreamp.YoutubePreamp",
    ),
    scalacOptions --= Seq(
      "-Wunused:params",
      "-Wunused:explicits",
      "-Wvalue-discard",
    ),
    scalacOptions ++= Seq(
      "-P:acyclic:force",
    ),
    autoCompilerPlugins := true,
    dependencyUpdatesFailBuild := true,
    scalaJSUseMainModuleInitializer := true,
    SassKeys.cssStyle := cssStyle,
    SassKeys.generateSourceMaps := !productionEnv,
    SassKeys.syntaxDetection := ForceScss,
    Compile / fullOptJS / scalaJSLinkerConfig ~= { _.withSourceMap(false) },
    Compile / moduleName := "YoutubePreamp",
    autoAPIMappings := true,
    buildInfoKeys := Seq[BuildInfoKey](name, version),
    buildInfoPackage := "com.kklorenzotesta.youtubepreamp",
    libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.1.0",
    libraryDependencies += "org.typelevel" %%% "cats-core" % "2.6.1",
    libraryDependencies += "com.lihaoyi" %%% "scalatags" % "0.9.4",
    libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-parser",
    ).map(_ % circeVersion),
    libraryDependencies += "io.circe" %%% "circe-generic-extras" % circeVersion,
    libraryDependencies += "com.lihaoyi" %% "acyclic" % "0.2.1" % "provided",
    jsDependencies += "org.webjars" % "jquery" % "3.3.1-2" / "jquery.min.js",
    jsDependencies += "org.webjars.bowergithub.kklorenzotesta" % "materialize" % "1.0.0-YT2" / "js/materialize.min.js",
    jsDependencies += ProvidedJS / "noUiSlider/nouislider.min.js",
    jsDependencies += ProvidedJS / "native.js",
    packageJSDependencies / skip := false,
    chromeSourceIcon := Some((Compile / resourceDirectory).value / "icon.png"),
    chromeJsBuild := jsBuildTask.value,
    chromeManifestContentScriptsMatches := List(
      "*://www.youtube.com/*",
      "*://m.youtube.com/*",
    ),
    chromeCssAssets := (Assets / sassify).value ++ List(
      (Assets / webJarsDirectory).value / "lib" / "materialize" / "noUiSlider" / "nouislider.css",
    ),
    chromeManifestContentScriptsCss := chromeCss.value
      .filter(_.getName.contains("wrapper")),
    chromeManifest / version := {
      val out = version.value
      if (out.contains(dynverSeparator.value)) {
        previousStableVersion.value
          .map(_.split('.').map(_.toInt))
          .map(l => l.init :+ (l.last + 1))
          .map(_.mkString("."))
          .getOrElse("0")
      } else {
        out
      }
    },
  )
