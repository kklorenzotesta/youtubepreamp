import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.{Files, StandardCopyOption, StandardOpenOption}

import sbt._
import Keys._
import com.typesafe.sbt.web.SbtWeb
import javax.imageio.ImageIO
import org.scalajs.sbtplugin.ScalaJSPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import org.scalajs.jsdependencies.sbtplugin.JSDependenciesPlugin
import org.scalajs.jsdependencies.sbtplugin.JSDependenciesPlugin.autoImport._
import play.api.libs.json._

object ChromeBundlerPlugin extends AutoPlugin {
  override def requires = ScalaJSPlugin && JSDependenciesPlugin && SbtWeb

  object autoImport {
    lazy val chrome = taskKey[File]("Bundles chrome files")
    lazy val chromeJs =
      taskKey[File]("Bundles the js files for the chrome extension")
    lazy val chromeJsBuild = taskKey[Attributed[File]]("Build the js file")
    lazy val chromeJsFolder =
      settingKey[File]("Folder for js files in the extension")
    lazy val chromeJsMainFile =
      settingKey[File]("Main js file for chrome extension")
    lazy val chromeJsDepsFile =
      settingKey[Option[File]]("Js deps file for chrome extension")
    lazy val chromeCss =
      taskKey[Seq[File]]("The css files bundled with the chrome extension")
    lazy val chromeCssAssets = taskKey[Seq[File]](
      "The css files provided by other sources that will be copied in the chrome extension",
    )
    lazy val chromeCssFolder =
      settingKey[File]("Folder for the css files in the extension")
    lazy val chromeManifest =
      taskKey[File]("Generates the chrome extension manifest")
    lazy val chromeManifestShortName =
      settingKey[Option[String]]("Short name for the chrome extension")
    lazy val chromeManifestDescription =
      settingKey[Option[String]]("Description for the chrome extension")
    lazy val chromeManifestHomepageUrl =
      settingKey[Option[String]]("Homepage Url for the chrome extension")
    lazy val chromeManifestDefaultLocale =
      settingKey[Option[String]]("Default locale for the chrome extension")
    lazy val chromeManifestContentScriptsMatches = settingKey[Seq[String]](
      "Specifies which pages this content script will be injected into",
    )
    lazy val chromeManifestContentScriptsCss =
      taskKey[Seq[File]]("The css files for this content script")
    lazy val chromeIcons = taskKey[Seq[File]]("Icons for the chrome extension")
    lazy val chromeIconSizes =
      settingKey[Seq[Int]]("Sizes for the generated chrome extension icons")
    lazy val chromeSourceIcon = settingKey[Option[File]](
      "Source icon from which all the app icons will be generate",
    )
  }

  import autoImport._

  override lazy val globalSettings: Seq[Setting[_]] = Seq(
    chromeManifestShortName := None,
    chromeManifestHomepageUrl := None,
    chromeManifestDefaultLocale := None,
    chromeIconSizes := List(16, 48, 128),
    chromeSourceIcon := None,
    chromeManifestContentScriptsMatches := Nil,
    chromeCssAssets := Nil,
  )

  override lazy val projectSettings: Seq[Setting[_]] = Seq(
    chromeManifest / name := (Compile / name).value,
    chromeManifest / version := (Compile / version).value,
    chrome / target := (Compile / target).value / "chrome",
    chromeJsFolder := (chrome / target).value / "js",
    chromeJsMainFile := (chrome / chromeJsFolder).value / ((Compile / fullOptJS / moduleName).value + ".js"),
    chromeJsDepsFile := Some(
      (chrome / chromeJsFolder).value / ((Compile / fullOptJS / moduleName).value + "-deps.js"),
    ),
    chromeJsBuild := (Compile / fullOptJS).value,
    chromeCssFolder := (chrome / target).value / "css",
    chromeManifestContentScriptsCss := (chrome / chromeCss).value,
    chromeIcons / target := (chrome / target).value / "icons",
    chromeManifest / artifactPath := (chrome / target).value / "manifest.json",
    chromeManifestDescription := {
      val projectDescription = (Compile / description).value
      if (projectDescription.isEmpty) {
        None
      } else {
        Some(projectDescription)
      }
    },
    chromeIcons := ChromeIcons(
      (chromeIcons / target).value,
      (chromeIcons / chromeSourceIcon).value,
      (chromeIcons / chromeIconSizes).value,
    ),
    chromeManifest := ChromeManifest(
      (chrome / target).value,
      (chromeManifest / artifactPath).value,
      (chromeJs / chromeJsMainFile).value,
      (chromeJs / chromeJsDepsFile).value,
      (chrome / chromeCss).value,
      (chromeManifest / chromeManifestContentScriptsCss).value,
      (chromeManifest / name).value,
      (chromeManifest / version).value,
      (chromeManifest / chromeManifestShortName).value,
      (chromeManifest / chromeManifestDescription).value,
      (chromeManifest / chromeManifestHomepageUrl).value,
      (chromeManifest / chromeManifestDefaultLocale).value,
      (chromeManifest / chromeIcons).value,
      (chromeManifest / chromeManifestContentScriptsMatches).value,
      streams.value.log,
    ),
    chromeJs := {
      val jsFile = (chromeJs / chromeJsBuild).value
      ChromeJs(
        (chromeJs / chromeJsFolder).value,
        (chromeJs / chromeJsMainFile).value,
        (chromeJs / chromeJsDepsFile).value,
        jsFile.data,
        jsFile.get(scalaJSSourceMap),
        (Compile / packageJSDependencies).value,
      )
    },
    chromeCss := {
      ChromeCss(
        (chromeCss / chromeCssFolder).value,
        (chromeCss / chromeCssAssets).value,
      )
    },
    chrome := {
      (chrome / chromeJs).value
      (chrome / chromeCss).value
      (chrome / chromeManifest).value
      (chrome / target).value
    },
  )

  super.globalSettings
}

object ChromeJs {
  def apply(
      targetFolder: File,
      targetJsFile: File,
      targetJsDepsFile: Option[File],
      jsFile: File,
      jsMapFile: Option[File],
      depsFile: File,
  ): File = {
    targetFolder.mkdirs()
    Files.copy(
      jsFile.toPath,
      targetJsFile.toPath,
      StandardCopyOption.REPLACE_EXISTING,
    )
    jsMapFile.foreach { mapFile =>
      if (mapFile.exists()) {
        Files.copy(
          mapFile.toPath,
          new File(targetFolder, mapFile.getName).toPath,
          StandardCopyOption.REPLACE_EXISTING,
        )
      }
    }
    targetJsDepsFile.foreach { outDeps =>
      if (depsFile.exists()) {
        Files.copy(
          depsFile.toPath,
          outDeps.toPath,
          StandardCopyOption.REPLACE_EXISTING,
        )
      } else {
        Files.write(
          outDeps.toPath,
          "".getBytes,
          StandardOpenOption.CREATE,
          StandardOpenOption.WRITE,
        )
      }
    }
    targetFolder
  }
}

object ChromeCss {
  def apply(targetFolder: File, assetsCss: Seq[File]): Seq[File] = {
    targetFolder.mkdirs()
    assetsCss.map { css =>
      val newCss = targetFolder / css.getName
      Files.copy(css.toPath, newCss.toPath, StandardCopyOption.REPLACE_EXISTING)
      newCss
    }
  }
}

object ChromeIcons {
  def apply(
      targetIconsFolder: File,
      sourceIcon: Option[File],
      iconSizes: Seq[Int],
  ): Seq[File] =
    sourceIcon
      .map { icon =>
        targetIconsFolder.mkdirs()
        iconSizes.map { size =>
          val output = targetIconsFolder / (icon.getName.substring(
            0,
            icon.getName.lastIndexOf('.'),
          ) + "-" + size + ".png")
          val image = ImageIO.read(icon)
          val scaledImage =
            new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB)
          val graphic = scaledImage.createGraphics()
          graphic.setRenderingHint(
            RenderingHints.KEY_INTERPOLATION,
            RenderingHints.VALUE_INTERPOLATION_BILINEAR,
          )
          graphic.drawImage(image, 0, 0, size, size, null)
          graphic.dispose()
          ImageIO.write(scaledImage, "png", output)
          output
        }
      }
      .getOrElse(Nil)
}

object ChromeManifest {
  def apply(
      chromeTargetFolder: File,
      artifactPath: File,
      mainJsFile: File,
      depsFile: Option[File],
      cssFiles: Seq[File],
      contentScriptCssFiles: Seq[File],
      name: String,
      version: String,
      shortName: Option[String],
      description: Option[String],
      homepageUrl: Option[String],
      defaultLocale: Option[String],
      icons: Seq[File],
      contentScriptMatches: Seq[String],
      log: Logger,
  ): File = {
    val iconsJs = if (icons.isEmpty) {
      log.warn("Extension icons are recommended")
      Nil
    } else {
      List(
        "icons" -> JsObject(
          icons
            .map { icon =>
              (ImageIO.read(icon).getWidth.toString, icon)
            }
            .map { case (size, icon) =>
              size -> JsString(relativePath(icon, chromeTargetFolder))
            },
        ),
      )
    }
    val matches = if (contentScriptMatches.isEmpty) {
      Nil
    } else {
      List("matches" -> JsArray(contentScriptMatches.map(JsString)))
    }
    val cssContentScript = if (contentScriptCssFiles.isEmpty) {
      Nil
    } else {
      List(
        "css" -> JsArray(
          contentScriptCssFiles
            .map(cssFile => JsString(relativePath(cssFile, chromeTargetFolder))),
        ),
      )
    }
    val webResources = List(
      "web_accessible_resources" -> JsArray(
        List(
          JsObject(
            List(
              "resources" ->
                JsArray(
                  (cssFiles
                    .map(cssFile =>
                      JsString(relativePath(cssFile, chromeTargetFolder)),
                    )
                    ++ icons.map(icon =>
                      JsString(relativePath(icon, chromeTargetFolder)),
                    )),
                ),
            ) ++ matches,
          ),
        ),
      ),
    )
    val manifest = JsObject(
      Seq(
        "manifest_version" -> JsNumber(3),
        "name" -> JsString(name),
        "version" -> JsString(version),
      ) ++ shortName
        .map(sn => List("short_name" -> JsString(sn)))
        .getOrElse(Nil)
        ++ description.map(d => List("description" -> JsString(d))).getOrElse {
          log.warn("Extension description is recommended")
          Nil
        }
        ++ homepageUrl
          .map(hu => List("homepage_url" -> JsString(hu)))
          .getOrElse(Nil)
        ++ defaultLocale
          .map(dl => List("default_locale" -> JsString(dl)))
          .getOrElse {
            log.warn("Extension default locale is recommended")
            Nil
          }
        ++ iconsJs
        ++ webResources
        ++ List(
          "content_scripts" -> JsArray(
            List(
              JsObject(
                List(
                  "js" -> JsArray(
                    depsFile
                      .map(df =>
                        List(JsString(relativePath(df, chromeTargetFolder))),
                      )
                      .getOrElse {
                        Nil
                      } ++ List(
                      JsString(relativePath(mainJsFile, chromeTargetFolder)),
                    ),
                  ),
                ) ++ matches ++ cssContentScript,
              ),
            ),
          ),
        ),
    )
    IO.write(artifactPath, Json.stringify(manifest))
    artifactPath
  }

  private def relativePath(file: File, relativeTo: File): String =
    relativeTo.toPath.relativize(file.toPath).toString.replace("\\", "/")
}
