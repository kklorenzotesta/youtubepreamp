# YoutubePreamp

[download chrome extension](https://chrome.google.com/webstore/detail/youtubepreamp/mcjbehbddnhmcecicjalhcigjhdccaac)
[download firefox extension](https://addons.mozilla.org/en-US/firefox/addon/youtubepreamp/)

Chrome and firefox extension that adds to every youtube video the controls of a simple sound system preamp. If you want to contribute, require support or request new features feel free to open an issue.

## Implementation

YoutubePreamp is implemented in Scala.js and requires Java 8 and sbt installed.

To compile the extension run `sbt chrome`, the extension will be created in the `target/chrome` folder. To create a production built run the same task with the environment variable `JS_ENV` set to `production` before starting sbt.

[scaladoc](https://kklorenzotesta.gitlab.io/youtubepreamp/com/kklorenzotesta/youtubepreamp/index.html)
