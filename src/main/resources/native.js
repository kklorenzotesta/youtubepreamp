"use strict";

function executeAttachShadow(elem, theMode) {
  return elem.attachShadow({
    mode: theMode
  });
}

function createEvent(name) {
  return new Event(name);
}

function createVerticalSlider(slider, minValue, maxValue, startValue, onUpdate) {
  noUiSlider.create(slider, {
    start: startValue,
    orientation: 'vertical',
    direction: 'rtl',
    step: 1,
    range: {
      'min': minValue,
      'max': maxValue
    },
    tooltips: false
  });
  slider.noUiSlider.on('update', value => onUpdate(parseInt(value[0])));
  return slider.noUiSlider;
}

function createXOverSettingsSlider(slider, startingValues, onUpdate) {
  noUiSlider.create(slider, {
    start: startingValues,
    range: {
      'min': [50, 5],
      '20%': [100, 5],
      '50%': [400, 50],
      '70%': [1000, 100],
      'max': [20000]
    },
    format: wNumb({
        decimals: 1
    }),
    pips: {
        mode: 'values',
        density: 3,
        values: [50, 100, 200, 300, 400, 1000, 20000]
    }
  });
  slider.noUiSlider.on('update', values => onUpdate(values.map(value => parseInt(value))));
  return slider.noUiSlider;
}
