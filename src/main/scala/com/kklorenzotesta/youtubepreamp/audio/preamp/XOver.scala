package com.kklorenzotesta.youtubepreamp.audio.preamp

import scala.collection.immutable.Seq

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import com.kklorenzotesta.youtubepreamp.audio.DisconnectableStage
import com.kklorenzotesta.youtubepreamp.audio.MulticastStage
import com.kklorenzotesta.youtubepreamp.audio.components.BandPassStage
import com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage
import com.kklorenzotesta.youtubepreamp.audio.preamp.XOver.XOverBand
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] acting as a 4 band crossover, with volumes and cuts, after which the bands are rejoined.
  *
  * @param output  the stage on which the rejoined signal will be sent
  * @param context the `AudioContext` from which `AudioNode`s will be created
  */
class XOver private (output: AudioStage)(implicit val context: AudioContext)
    extends AudioStage {
  private val xOverVolumes: Map[XOverBand, VolumeStage] =
    XOverBand.bands.map(band => (band, VolumeStage() ->> output)).toMap
  private val xOverDisconnectables: Map[XOverBand, DisconnectableStage] =
    xOverVolumes.map { case (band, volume) =>
      (band, DisconnectableStage() ->> volume)
    }
  private val xOverBands: Map[XOverBand, BandPassStage] =
    xOverDisconnectables.map { case (band, volume) =>
      (band, XOverBand(band) ->> volume)
    }
  override val inputNode: AudioNode =
    (VolumeStage(
      0.6,
    ) ->> MulticastStage() ->> xOverBands.values.toList).inputNode

  /** Returns a [[DisconnectableStage]] that allows to connect/disconnect the specified band
    *
    * @param band the band for which the node will be obtained
    * @return a [[DisconnectableStage]] allowing to connect/disconnect the given band
    */
  def getDisconnectableBand(band: XOverBand): DisconnectableStage =
    xOverDisconnectables(band)

  /** Returns a [[com.kklorenzotesta.youtubepreamp.audio.components.BandPassStage]] that allows to change the frequency of the specified band.
    *
    * @param band the band for which the node will be obtained
    * @return a BandPassStage allowing to chenge the frequency of the given band
    */
  def getBand(band: XOverBand): BandPassStage = xOverBands(band)

  /** Returns a [[com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage]] that allows to change the volume
    * of the specified band
    *
    * @param band the band for which the node will be obtained
    * @return a VolumeStage allowing to change the volume of the given band
    */
  def getVolume(band: XOverBand): VolumeStage = xOverVolumes(band)
}

/** Provides a builder for [[XOver]]s and the bands of the crossover */
object XOver {

  /** Specifies the range of a band of a crossover
    *
    * @param defaultFrom the lower frequency of the band
    * @param defaultTo   the upper frequency of the band
    */
  sealed abstract class XOverBand private (
      val defaultFrom: Int,
      val defaultTo: Int,
  )

  /** Provides a builder for the bands and the default bands specification
    */
  object XOverBand {

    /** Specifies the sub band of the crossover */
    case object Sub extends XOverBand(0, Low.defaultFrom)

    /** Specifies the low band of the crossover */
    case object Low extends XOverBand(100, Mid.defaultFrom)

    /** Specifies the mid band of the crossover */
    case object Mid extends XOverBand(250, High.defaultFrom)

    /** Specifies the high band of the crossover */
    case object High extends XOverBand(4000, 22050)

    /** A [[scala.collection.immutable.Seq]] of all the bands of a [[XOver]] */
    val bands: Seq[XOverBand] = List(Sub, Low, Mid, High)

    /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for producing a
      * crossover band.
      *
      * @param band    the band to be created
      * @param context the `AudioContext` from which `AudioNode`s will be created
      * @return a builder for a band chain of a crossover
      */
    private[XOver] def apply(band: XOverBand)(implicit
        context: AudioContext,
    ): AudioStageBuilder[BandPassStage, AudioStage] =
      BandPassStage(band.defaultFrom, band.defaultTo, order = 2)
  }

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[XOver]]s
    *
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for crossovers
    */
  def apply()(implicit
      context: AudioContext,
  ): AudioStageBuilder[XOver, AudioStage] = new XOver(_)

}
