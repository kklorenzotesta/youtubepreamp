package com.kklorenzotesta.youtubepreamp.audio.preamp

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.SimpleNodeStage
import com.kklorenzotesta.youtubepreamp.audio.MulticastStage
import com.kklorenzotesta.youtubepreamp.audio.UnsafeLoopStage
import com.kklorenzotesta.youtubepreamp.audio.components.FilterStage.HighPassStage
import com.kklorenzotesta.youtubepreamp.audio.components.LimiterStage
import com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] acting as a delay effect for the input
  *
  * @param output                  the stage that will receive the output of the delay effect
  * @param maxDelayTime            the maximum delay time allowed
  * @param highPassFreq            the frequency under which the input will be cut
  * @param feedbackFilterFrequency the frequency for the cut of the feedback filter
  * @param context                 the `AudioContext` from which `AudioNode`s will be created
  */
class Delay private (
    output: AudioStage,
    maxDelayTime: Int,
    highPassFreq: Int,
    feedbackFilterFrequency: Int,
)(implicit val context: AudioContext)
    extends AudioStage {
  private val feedbackFilterNode = context.createBiquadFilter()
  feedbackFilterNode.frequency.value = feedbackFilterFrequency.toDouble
  private val delayNode = context.createDelay(maxDelayTime)

  /** A [[com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage]] controlling the amount of feedback
    * of the delay
    */
  val feedback: VolumeStage = VolumeStage() ->> LimiterStage() ->>
    SimpleNodeStage(feedbackFilterNode) ->> UnsafeLoopStage() ->> delayNode
  override val inputNode: AudioNode =
    (HighPassStage(highPassFreq) ->> SimpleNodeStage(delayNode) ->>
      MulticastStage() ->> List(feedback, output)).inputNode

  /** Changes the delay time
    *
    * @param time the new delay time
    */
  def updateDelayTime(time: Double): Unit = {
    delayNode.delayTime.value = time
  }
}

/** Provides a builder for [[Delay]]s */
object Delay {

  /** The default high pass frequency for the delays */
  private val defaultHighPassFreq: Int = 200

  /** The default delay filter frequency for the delay feedback */
  private val defaultDelayFilterFreq: Int = 2500

  /** The default maximum delay time accepted */
  private val defaultMaxDelayTime: Int = 179

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[Delay]]s configured
    * with the given parameters.
    *
    * @param maxDelayTime    the maximum delay time for the created delays
    * @param highPassFreq    the frequency under which the input will be cut by the created delays
    * @param delayFilterFreq the frequency for the cut of the feedback filters of the created delays
    * @param context         the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for delays
    */
  def apply(
      maxDelayTime: Int = defaultMaxDelayTime,
      highPassFreq: Int = defaultHighPassFreq,
      delayFilterFreq: Int = defaultDelayFilterFreq,
  )(implicit context: AudioContext): AudioStageBuilder[Delay, AudioStage] =
    new Delay(_, maxDelayTime, highPassFreq, delayFilterFreq)
}
