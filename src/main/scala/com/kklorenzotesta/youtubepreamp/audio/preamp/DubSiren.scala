package com.kklorenzotesta.youtubepreamp.audio.preamp

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import com.kklorenzotesta.youtubepreamp.audio.components.OscillatorStage
import com.kklorenzotesta.youtubepreamp.audio.components.OscillatorStage.Square
import com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] acting as a dub siren
  *
  * @param output  the stage to which the generated siren sound will be sent
  * @param context the `AudioContext` from which `AudioNode`s will be created
  */
class DubSiren private (output: AudioStage)(implicit val context: AudioContext)
    extends AudioStage {

  /** An [[com.kklorenzotesta.youtubepreamp.audio.components.OscillatorStage]] acting as the main oscillator
    * for the siren
    */
  val mainOscillator: OscillatorStage =
    OscillatorStage(200, Square) ->> VolumeStage(0.15) ->> output
  private val lfoSineVolume =
    VolumeStage(DubSiren.defaultLfoVolume) ->> mainOscillator.LFOStage
  private val lfoSineOscillator: OscillatorStage =
    OscillatorStage(1) ->> lfoSineVolume
  private val lfoSquareVolume =
    VolumeStage(DubSiren.defaultLfoVolume) ->> mainOscillator.LFOStage
  private val lfoSquareOscillator: OscillatorStage =
    OscillatorStage(1, Square) ->> lfoSquareVolume
  override val inputNode: AudioNode = mainOscillator.inputNode

  /** Updates the frequency of the LFOs with the given value
    *
    * @param frequency the new frequency for the LFOs
    */
  def updateLFO(frequency: Double): Unit = {
    lfoSineOscillator.updateFrequency(frequency)
    lfoSquareOscillator.updateFrequency(frequency)
  }

  /** The mix percentage between the sine LFO and the square LFO. The value will be bound to the range [0,1],
    * where 0 represents only the sine LFO, 1 only the square LFO and 0.5 both at their full volume.
    *
    * @param percent the percentage of mix between sine and square LFO
    */
  def updateSineSquareMix(percent: Double): Unit = {
    val rangedValue = math.max(math.min(percent, 1.0), 0)
    if (rangedValue == 0.5) {
      lfoSineVolume.setVolume(DubSiren.defaultLfoVolume)
      lfoSquareVolume.setVolume(DubSiren.defaultLfoVolume)
    } else if (rangedValue > 0.5) {
      lfoSquareVolume.setVolume(DubSiren.defaultLfoVolume)
      lfoSineVolume.setVolume(
        DubSiren.defaultLfoVolume * (1 - rangedValue) / 0.5,
      )
    } else {
      lfoSineVolume.setVolume(DubSiren.defaultLfoVolume)
      lfoSquareVolume.setVolume(DubSiren.defaultLfoVolume * rangedValue / 0.5)
    }
  }
}

/** Provides a builder for [[DubSiren]]s */
object DubSiren {

  /** The default volume for the LFOs of the sirens */
  private val defaultLfoVolume = 100.0

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[DubSiren]]s
    *
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for dub sirens
    */
  def apply()(implicit
      context: AudioContext,
  ): AudioStageBuilder[DubSiren, AudioStage] = new DubSiren(_)
}
