package com.kklorenzotesta.youtubepreamp.audio.preamp

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import com.kklorenzotesta.youtubepreamp.audio.DisconnectableStage
import com.kklorenzotesta.youtubepreamp.audio.MulticastStage
import com.kklorenzotesta.youtubepreamp.audio.components.ToggleStage
import com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] acting as a full preamp.
  *
  * @param output  the stage that will receive the audio processed by the preamp
  * @param context the `AudioContext` from which `AudioNode`s will be created
  */
class Preamp private (output: AudioStage)(implicit val context: AudioContext)
    extends AudioStage {

  /** A [[com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage]] controlling the volume of the delay
    */
  val delayVolume: VolumeStage = VolumeStage() ->> output

  /** The [[Delay]] of the preamp */
  val delay: Delay = Delay() ->> delayVolume

  /** A [[DisconnectableStage]] that controls if the delay receives the preamp input or not */
  val delaySwitch: DisconnectableStage = DisconnectableStage(
    startEnabled = false,
  ) ->> delay

  /** The [[XOver]] of the preamp */
  val xOver: XOver = XOver() ->> output

  /** A [[com.kklorenzotesta.youtubepreamp.audio.components.ToggleStage]] that enables to skip the crossover.
    * The main stage of the toggle is the crossover.
    */
  val xOverSwitch: ToggleStage = ToggleStage() ->> ((xOver, output))

  /** A [[com.kklorenzotesta.youtubepreamp.audio.components.VolumeStage]] controlling the volume of the dub siren */
  val dubSirenVolume: VolumeStage = VolumeStage() ->> MulticastStage() ->> List(
    output,
    delay,
  )

  /** A [[DisconnectableStage]] controlling if the dub siren is playing or not */
  val dubSirenSwitch: DisconnectableStage = DisconnectableStage(
    startEnabled = false,
  ) ->> dubSirenVolume

  /** The [[DubSiren]] of the preamp */
  val dubSiren: DubSiren = DubSiren() ->> dubSirenSwitch
  override val inputNode: AudioNode =
    (MulticastStage() ->> List(delaySwitch, xOverSwitch)).inputNode
}

/** Provides a builder for [[Preamp]]s
  */
object Preamp {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[Preamp]]s
    *
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for preamps
    */
  def apply()(implicit
      context: AudioContext,
  ): AudioStageBuilder[Preamp, AudioStage] = new Preamp(_)
}
