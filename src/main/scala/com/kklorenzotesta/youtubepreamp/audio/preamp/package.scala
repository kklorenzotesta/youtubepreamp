package com.kklorenzotesta.youtubepreamp.audio

/** Provides the high level stages of a preamp and the preamp stage itself */
package object preamp {}
