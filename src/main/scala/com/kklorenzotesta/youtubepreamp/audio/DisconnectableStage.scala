package com.kklorenzotesta.youtubepreamp.audio

import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.GainNode

/** An [[AudioStage]] providing the ability to disconnect and reconnect the node to the chain. <b>There is no guarantee
  * that when a disconnect or connect operation returns the operation is effectively completed.</b>
  *
  * @param output         the stage to which the previous stage will be connected/disconnected
  * @param startEnabled   true if this stage must start connected to the next stage, false otherwise
  * @param transitionTime the milliseconds that will take the fade in/out operations
  * @param context        the `AudioContext` from which `AudioNode`s will be created
  */
class DisconnectableStage private (
    output: AudioStage,
    startEnabled: Boolean,
    transitionTime: Double,
)(implicit val context: AudioContext)
    extends AudioStage {
  override val inputNode: GainNode = context.createGain()
  inputNode.gain.value = if (startEnabled) 1 else 0
  inputNode.connect(output.inputNode)

  /** Enables/disables the connection to the next stage. The connection/disconnection will be performed by fading the
    * volume to audio noise while performing the disconnection. <b>There is no guarantee
    * that when this method returns the operation is effectively completed.</b>
    *
    * @param enabled true if the connection to the next stage must be enabled, false otherwise
    */
  def setEnabled(enabled: Boolean): Unit =
    if (enabled) {
      inputNode.gain.setTargetAtTime(1, context.currentTime, transitionTime)
    } else {
      inputNode.gain.setTargetAtTime(0, context.currentTime, transitionTime)
    }
}

/** Provides a builder for [[DisconnectableStage]]s
  */
object DisconnectableStage {
  private val defaultTransitionTime: Double = 0.015

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[DisconnectableStage]]s.
    *
    * @param startEnabled   true if the created stages will start connected.
    * @param transitionTime the time taken by the fade in/out operation in the created stages.
    * @param context        the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for [[DisconnectableStage]]s
    */
  def apply(
      startEnabled: Boolean = true,
      transitionTime: Double = defaultTransitionTime,
  )(implicit
      context: AudioContext,
  ): AudioStageBuilder[DisconnectableStage, AudioStage] =
    new DisconnectableStage(_, startEnabled, transitionTime)
}
