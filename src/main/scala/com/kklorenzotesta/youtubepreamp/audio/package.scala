package com.kklorenzotesta.youtubepreamp

/** Contains abstractions for the creation of a chain of audio manipulation and generation.
  *
  * [[audio.AudioStage]] represents a generic stage in a chain of audio processing. In this package are provided the
  * basic building blocks for audio processing, while [[audio.components]] contains more advanced stages.
  * [[audio.preamp]] contains the high level stages used directly by the preamp.
  *
  * The creating of a chain of audio stages is performed through a DSL provided by the [[audio.AudioStage]] companion
  * object. For example a simple chain that allows to mute or change the volume of the sound from `audioNode`
  * can be built like this:
  * {{{
  *   MediaSourceStage(audioNode) ->> DisconnectableStage() ->> VolumeStage() ->> DestinationStage() ->> output
  * }}}
  */
package object audio {}
