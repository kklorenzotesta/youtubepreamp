package com.kklorenzotesta.youtubepreamp.audio

import scala.collection.immutable.Seq

import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.GainNode

/** An [[AudioStage]] that directs his input to multiple output stages.
  *
  * @param outputs the [[AudioStage]]s to which the input will be equally directed
  * @param context the `AudioContext` from which `AudioNode`s will be created
  */
class MulticastStage private (outputs: Seq[AudioStage])(implicit
    val context: AudioContext,
) extends AudioStage {
  override val inputNode: GainNode = context.createGain()
  inputNode.gain.value = 1
  outputs.foreach(stage => inputNode.connect(stage.inputNode))
}

/** Provides a builder for [[MulticastStage]]s
  */
object MulticastStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[MulticastStage]]s given
    * a [[scala.collection.immutable.Seq]] of [[AudioStage]]s in which the previous stage output of the chain will be directed.
    *
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for [[MulticastStage]]s
    */
  def apply()(implicit
      context: AudioContext,
  ): AudioStageBuilder[MulticastStage, Seq[AudioStage]] = new MulticastStage(_)
}
