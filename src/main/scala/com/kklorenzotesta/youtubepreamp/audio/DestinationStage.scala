package com.kklorenzotesta.youtubepreamp.audio

import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import org.scalajs.dom.raw.AudioDestinationNode

/** An [[AudioStage]] to be used as last stage for a chain of stages
  *
  * @param destination the `AudioDestinationNode` in which the chain will end
  */
class DestinationStage private (destination: AudioDestinationNode)
    extends AudioStage {
  override val inputNode: AudioDestinationNode = destination
}

/** Provides a builder for [[DestinationStage]]s */
object DestinationStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] that will create
    * a [[DestinationStage]] to be used as last node of chain.
    *
    * @return a builder for a terminal stage given the `AudioDestinationNode`
    */
  def apply(): AudioStageBuilder[DestinationStage, AudioDestinationNode] =
    new DestinationStage(_)
}
