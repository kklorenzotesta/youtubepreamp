package com.kklorenzotesta.youtubepreamp.audio

import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.HTMLMediaElement
import org.scalajs.dom.raw.MediaElementAudioSourceNode

/** An [[AudioStage]] to be used as starting stage of chain.
  *
  * @param source  the `HTMLMediaElement` from which the chain starts
  * @param output  the first stage in which the source audio will go
  * @param context the `AudioContext` from which `AudioNode`s will be created
  */
class MediaSourceStage private (source: HTMLMediaElement, output: AudioStage)(
    implicit val context: AudioContext,
) extends AudioStage {
  override val inputNode: MediaElementAudioSourceNode =
    context.createMediaElementSource(source)
  inputNode.connect(output.inputNode)
}

/** Provides a builder for [[MediaSourceStage]]s
  */
object MediaSourceStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for a [[MediaSourceStage]].
    *
    * @param source  the `HTMLMediaElement` from which the chain will start
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for a starting stage of a chain
    */
  def apply(source: HTMLMediaElement)(implicit
      context: AudioContext,
  ): AudioStageBuilder[MediaSourceStage, AudioStage] =
    new MediaSourceStage(source, _)
}
