package com.kklorenzotesta.youtubepreamp.audio

import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] that outputs directly into an `AudioNode` instead of an AudioStage, this allows to
  * create loops in the chain of stages.
  *
  * @param node the node in which the output will go
  */
class UnsafeLoopStage private (node: AudioNode) extends AudioStage {
  override val inputNode: AudioNode = node
}

/** Provides a builder for [[UnsafeLoopStage]]s
  */
object UnsafeLoopStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[UnsafeLoopStage]],
    * allowing to create loops in the chain
    *
    * @return a builder for an [[UnsafeLoopStage]]
    */
  def apply(): AudioStageBuilder[UnsafeLoopStage, AudioNode] =
    new UnsafeLoopStage(_)
}
