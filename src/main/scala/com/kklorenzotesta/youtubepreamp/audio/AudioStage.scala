package com.kklorenzotesta.youtubepreamp.audio

import cats._
import cats.data._
import org.scalajs.dom.raw.AudioNode

/** Represents an audio node or a chain of nodes treated as a unit */
trait AudioStage {

  /** The first `AudioNode` of this stage, so that other chains can output in this stage. */
  val inputNode: AudioNode
}

/** Provides utilities for simply build chains of [[AudioStage]]s */
object AudioStage {

  /** An AudioStageBuilder is a function that creates a `Stage` that outputs in the given `Output`
    *
    * @tparam Stage  the type of the created [[AudioStage]]
    * @tparam Output the type of the object in which the output of the created stage will go
    */
  type AudioStageBuilder[Stage <: AudioStage, -Output] =
    Kleisli[Id, Output, Stage]

  /** Converts a function from `Output` to `State` in an
    * [[AudioStageBuilder]], the function must have the right
    * semantic.
    *
    * @param f the function to be converted
    * @tparam Stage  the type of the produced [[AudioStage]]
    * @tparam Output the type of the object in which the output of the created stage will go
    * @return the equivalent [[AudioStageBuilder]]
    */
  implicit def builderFrom[Stage <: AudioStage, Output](
      f: Output => Stage,
  ): AudioStageBuilder[Stage, Output] =
    Reader(f)

  /** Provides a DSL to construct chains of [[AudioStage]].
    *
    * @param builder the [[AudioStageBuilder]] from which the chain will go on
    * @tparam Stage  the type of the produced [[AudioStage]]
    * @tparam Output the type of the object in which the output of the created stage will go
    */
  implicit class AudioStageBuilderOps[Stage <: AudioStage, -Output](
      builder: AudioStageBuilder[Stage, Output],
  ) {

    /** Completes the chain by using the final output to construct the stage
      *
      * @param output the object in which the output of the stage will go
      * @return the built `Stage`
      */
    def ->>(output: Output): Stage = builder(output)

    /** Extends the chain by combining two [[AudioStageBuilder]].
      *
      * @param nextBuilder the successive builder in the chain, the produced stages will be connected when using the
      *                    resulting builder.
      * @tparam MiddleStage the stage that will connect to the new output
      * @tparam NewOutput   the new type for the object in which the combined output will go
      * @return a builder concatenating the two builders
      */
    def ->>[MiddleStage <: Output with AudioStage, NewOutput](
        nextBuilder: AudioStageBuilder[MiddleStage, NewOutput],
    ): AudioStageBuilder[Stage, NewOutput] =
      builder compose Functor[AudioStageBuilder[*, NewOutput]]
        .widen[MiddleStage, Output](nextBuilder)
  }

  /** An [[AudioStage]] used internally by some kind of stages that simply wraps an [[AudioNode]]
    *
    * @param node   the wrapped node
    * @param output the [[AudioStage]] to which the node will be connected
    */
  private[audio] class SimpleNodeStage private (
      node: AudioNode,
      output: AudioStage,
  ) extends AudioStage {
    override val inputNode: AudioNode = node
    node.connect(output.inputNode)
  }

  /** Provides a builder for [[SimpleNodeStage]]s
    */
  private[audio] object SimpleNodeStage {

    /** Creates an [[AudioStageBuilder]] that produce am [[SimpleNodeStage]]
      *
      * @param node the node that will be wrapped when the stage is created
      * @return the builder for the wrapping stage
      */
    def apply(node: AudioNode): AudioStageBuilder[SimpleNodeStage, AudioStage] =
      new SimpleNodeStage(node, _)
  }

}
