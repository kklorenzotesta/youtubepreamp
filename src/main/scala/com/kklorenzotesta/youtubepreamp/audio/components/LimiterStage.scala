package com.kklorenzotesta.youtubepreamp.audio.components

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.DynamicsCompressorNode

/** An [[AudioStage]] acting as a limiter.
  *
  * @param output    the stage that will be receive the limited output
  * @param threshold the threshold of the limiter
  * @param knee      the knee of the limiter
  * @param ratio     the ratio of the limiter
  * @param attack    the attack of the limiter
  * @param release   the release of the limiter
  * @param context   the `AudioContext` from which `AudioNode`s will be created
  */
class LimiterStage private (
    output: AudioStage,
    threshold: Double,
    knee: Double,
    ratio: Double,
    attack: Double,
    release: Double,
)(implicit context: AudioContext)
    extends AudioStage {
  override val inputNode: DynamicsCompressorNode =
    context.createDynamicsCompressor()
  inputNode.threshold.value = threshold
  inputNode.knee.value = knee
  inputNode.ratio.value = ratio
  inputNode.attack.value = attack
  inputNode.release.value = release
  inputNode.connect(output.inputNode)
}

/** Provides a builder for [[LimiterStage]]s
  */
object LimiterStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[LimiterStage]]s
    * configured by the parameters.
    *
    * @param threshold the threshold of the generated limiters
    * @param knee      the knee of the generated limiters
    * @param ratio     the ratio of the generated limiters
    * @param attack    the attack of the generated limiters
    * @param release   the release of the generated limiters
    * @param context   the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for [[LimiterStage]]s
    */
  def apply(
      threshold: Double = 0,
      knee: Double = 0,
      ratio: Double = 20,
      attack: Double = 0.005,
      release: Double = 0.05,
  )(implicit
      context: AudioContext,
  ): AudioStageBuilder[LimiterStage, AudioStage] =
    new LimiterStage(_, threshold, knee, ratio, attack, release)
}
