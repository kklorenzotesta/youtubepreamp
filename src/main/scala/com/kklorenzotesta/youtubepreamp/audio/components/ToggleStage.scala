package com.kklorenzotesta.youtubepreamp.audio.components

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import com.kklorenzotesta.youtubepreamp.audio.DisconnectableStage
import com.kklorenzotesta.youtubepreamp.audio.MulticastStage
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] providing the ability to send the output to one of two different stages and choose
  * when to change stage.
  *
  * @param mainStage        the first stage that will be sent the output to
  * @param alternativeStage the alternative stage to which the output will be sent when the main stage is disconnected
  * @param context          the `AudioContext` from which `AudioNode`s will be created
  */
class ToggleStage private (mainStage: AudioStage, alternativeStage: AudioStage)(
    implicit val context: AudioContext,
) extends AudioStage {
  private val mainStageDisconnectableStage = DisconnectableStage() ->> mainStage
  private val alternativeStageDisconnectableStage =
    DisconnectableStage() ->> alternativeStage
  override val inputNode: AudioNode = (MulticastStage() ->>
    List(
      mainStageDisconnectableStage,
      alternativeStageDisconnectableStage,
    )).inputNode

  /** Updates to which stage the output will be sent. <b>There is no guarantee
    * that when this method returns the operation is effectively completed.</b>
    *
    * @param onMain true if the output must be sent to the main stage, false if must be sent to the alternative one
    */
  def setOnMain(onMain: Boolean): Unit = {
    mainStageDisconnectableStage.setEnabled(onMain)
    alternativeStageDisconnectableStage.setEnabled(!onMain)
  }
}

/** Provides a builder for [[ToggleStage]]s
  */
object ToggleStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[ToggleStage]]s,
    * that will require two different stages as the next step of the chain.
    *
    * @param context the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for ToggleStages
    */
  def apply()(implicit
      context: AudioContext,
  ): AudioStageBuilder[ToggleStage, (AudioStage, AudioStage)] =
    (pair: (AudioStage, AudioStage)) => new ToggleStage(pair._1, pair._2)
}
