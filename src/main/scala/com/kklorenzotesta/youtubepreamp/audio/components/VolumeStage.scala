package com.kklorenzotesta.youtubepreamp.audio.components

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.GainNode

/** An [[AudioStage]] providing the ability to change the volume on the audio received from the input.
  *
  * @param output         the AudioStage that will receive the input with the volume changed
  * @param startingVolume the initial volume in percentage
  * @param context        the `AudioContext` from which `AudioNode`s will be created
  */
class VolumeStage private (output: AudioStage, startingVolume: Double)(implicit
    val context: AudioContext,
) extends AudioStage {
  override val inputNode: GainNode = context.createGain()
  inputNode.gain.value = startingVolume
  inputNode.connect(output.inputNode)

  /** Sets the given percent of volume as the volume for the output of this stage.
    *
    * @param percent the percent of the volume, with 0 equal to no sound and 1 equal to skip this stage
    */
  def setVolume(percent: Double): Unit = inputNode.gain.value = percent
}

/** Provides a builder for [[VolumeStage]]s */
object VolumeStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] for [[VolumeStage]]s that will
    * be created with the given initial percentage volume.
    *
    * @param startingVolume the initial volume for the created stages in percentage
    * @param context        the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for VolumeStages
    */
  def apply(startingVolume: Double = 1)(implicit
      context: AudioContext,
  ): AudioStageBuilder[VolumeStage, AudioStage] =
    new VolumeStage(_, startingVolume)
}
