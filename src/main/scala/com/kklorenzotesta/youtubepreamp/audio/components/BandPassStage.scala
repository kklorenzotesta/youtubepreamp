package com.kklorenzotesta.youtubepreamp.audio.components

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilderOps
import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.AudioNode

/** An [[AudioStage]] representing a band pass filter.
  *
  * @param output     the stage that will received the filtered output
  * @param lowerBound the lower frequency of the band pass filter
  * @param upperBound the upper frequency of the band pass filter
  * @param qFactor    the q factor of the filter
  * @param order      the order of the filter
  * @param context    the `AudioContext` from which `AudioNode`s will be created
  */
class BandPassStage(
    output: AudioStage,
    lowerBound: Int,
    upperBound: Int,
    qFactor: Double,
    order: Int,
)(implicit val context: AudioContext)
    extends AudioStage {
  private val lowPassFilters: Seq[FilterStage] =
    (1 until order)
      .map(_ => FilterStage.LowPassStage(upperBound, qFactor))
      .scanRight(FilterStage.LowPassStage(upperBound, qFactor) ->> output)(
        _ ->> _,
      )
  private val highPassFilters =
    (1 to order)
      .map(_ => FilterStage.HighPassStage(lowerBound, qFactor))
      .scanRight(lowPassFilters.head)(_ ->> _)
      .init

  override val inputNode: AudioNode = highPassFilters.head.inputNode

  /** Set the frequency passed by this filter
    *
    * @param lowerBound the frequency under which the signal will be cut
    * @param upperBound the frequency over which the signal will be cut
    */
  def setRange(lowerBound: Int, upperBound: Int): Unit = {
    setUpperBound(upperBound)
    setLowerBound(lowerBound)
  }

  /** Sets the frequency under which the signal will be cut */
  def setLowerBound(lowerBound: Int): Unit =
    highPassFilters.foreach(_.setFrequency(lowerBound))

  /** Sets the frequency over which the signal will be cut */
  def setUpperBound(upperBound: Int): Unit =
    lowPassFilters.foreach(_.setFrequency(upperBound))
}

/** Provides a builder for band pass filter stages */
object BandPassStage {

  /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] that creates stages which
    * acts as band pass filters.
    *
    * @param lowerBound the frequency under which the signal will be cut
    * @param upperBound the frequency over which the signal will be cut
    * @param qFactor    the q factor of the filter
    * @param order      the order of the filter
    * @param context    the `AudioContext` from which `AudioNode`s will be created
    * @return a builder for band pass filter stages
    */
  def apply(
      lowerBound: Int,
      upperBound: Int,
      qFactor: Double = FilterStage.defaultQFactor,
      order: Int = 1,
  )(implicit
      context: AudioContext,
  ): AudioStageBuilder[BandPassStage, AudioStage] =
    new BandPassStage(_, lowerBound, upperBound, qFactor, order)
}
