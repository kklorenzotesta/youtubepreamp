package com.kklorenzotesta.youtubepreamp.audio.components

import com.kklorenzotesta.youtubepreamp.audio.AudioStage
import com.kklorenzotesta.youtubepreamp.audio.AudioStage._
import org.scalajs.dom.raw.AudioContext
import org.scalajs.dom.raw.BiquadFilterNode

/** An [[AudioStage]] that acts either as an high pass or low pass filter
  *
  * @param output     the stage that will received the filtered output
  * @param filterType the type of the filter
  * @param freq       the frequency of the filter
  * @param q          the q factor of the filter
  * @param context    the `AudioContext` from which `AudioNode`s will be created
  */
class FilterStage private (
    output: AudioStage,
    filterType: FilterStage.FilterType,
    freq: Int,
    q: Double,
)(implicit context: AudioContext)
    extends AudioStage {
  override val inputNode: BiquadFilterNode = context.createBiquadFilter()
  inputNode.`type` = filterType.stringName
  inputNode.frequency.value = freq.toDouble
  inputNode.detune.value = 0
  inputNode.Q.value = q
  inputNode.connect(output.inputNode)

  /** Sets the given frequency as the frequency for the filtering.
    *
    * @param frequency the new frequency
    */
  def setFrequency(frequency: Int): Unit =
    inputNode.frequency.value = frequency.toDouble
}

/** Provides builders for the main kind of filters
  */
object FilterStage {

  /** The default q factor for the filter builders
    */
  private[components] val defaultQFactor: Double = 0.707

  /** Represents a type of filter
    *
    * @param stringName the string identifying the type of filter
    */
  private[components] sealed class FilterType(
      private[FilterStage] val stringName: String,
  )

  /** Represents a filter type of type low pass
    */
  private[components] case object LowPass extends FilterType("lowpass")

  /** Represents a filter type of type high pass
    */
  private[components] case object HighPass extends FilterType("highpass")

  /** Provides a builder for low pass filter stages
    */
  object LowPassStage {

    /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] that creates stages which
      * acts as low pass filters.
      *
      * @param frequency the frequency over which the signal will be cut
      * @param qFactor   the q factor of the filter
      * @param context   the `AudioContext` from which `AudioNode`s will be created
      * @return a builder for low pass filter stages
      */
    def apply(frequency: Int, qFactor: Double = FilterStage.defaultQFactor)(
        implicit context: AudioContext,
    ): AudioStageBuilder[FilterStage, AudioStage] =
      new FilterStage(_, LowPass, frequency, qFactor)
  }

  /** Provides a builder for high pass filter stages
    */
  object HighPassStage {

    /** Creates an [[com.kklorenzotesta.youtubepreamp.audio.AudioStage.AudioStageBuilder]] that creates stages which
      * acts as high pass filters.
      *
      * @param frequency the frequency under which the signal will be cut
      * @param qFactor   the q factor of the filter
      * @param context   the `AudioContext` from which `AudioNode`s will be created
      * @return a builder for high pass filter stages
      */
    def apply(frequency: Int, qFactor: Double = FilterStage.defaultQFactor)(
        implicit context: AudioContext,
    ): AudioStageBuilder[FilterStage, AudioStage] =
      new FilterStage(_, HighPass, frequency, qFactor)
  }
}
