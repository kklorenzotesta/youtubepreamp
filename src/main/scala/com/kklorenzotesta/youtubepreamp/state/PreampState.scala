package com.kklorenzotesta.youtubepreamp.state

/** Contains all the information about a possible state of the preamp */
case class PreampState(
    closed: Boolean = false,
    lastVersion: String = "",
    delayState: DelayState = DelayState(),
    sirenState: SirenState = SirenState(),
    xoverState: XOverState = XOverState(),
)
