package com.kklorenzotesta.youtubepreamp.state

/** Contains all the information about a possible state of the dub siren */
case class SirenState(
    frequency: Int = 390,
    lfo: Int = 26,
    sineSquare: Int = 50,
    volume: Int = 40,
    closed: Boolean = false,
)
