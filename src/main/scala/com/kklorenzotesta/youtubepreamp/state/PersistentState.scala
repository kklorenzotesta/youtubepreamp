package com.kklorenzotesta.youtubepreamp.state

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._
import io.circe.syntax._
import org.scalajs.dom.ext.LocalStorage
import org.scalajs.dom.ext.Storage

/** Provides access to the persistent state of the Preamp across all tabs
  *
  * @param storage the `Storage` holding the persistent state
  */
class PersistentState(private[this] val storage: Storage) {

  import PersistentState._

  val initialState = read

  /** Returns the current [[PreampState]] */
  def read: PreampState =
    storage(storageKey)
      .flatMap(decode[PreampState](_).toOption)
      .getOrElse({
        val state = PreampState()
        write(state)
        state
      })

  /** Persists the given [[PreampState]] */
  def write(state: PreampState): Unit =
    storage.update(storageKey, state.asJson.noSpaces)

  /** Updates the current [[PreampState]] according to the given function */
  def update(f: PreampState => PreampState): Unit =
    write(f(read))

}

/** Provides factory methods for create a [[PersistentState]] */
object PersistentState {

  /** Custom configuration for circe serialization/deserialization */
  implicit val customCirceConfiguration: Configuration =
    Configuration.default.withDefaults

  /** The key used in the `localStorage` to persist the information about the preamp */
  private val storageKey = "com.kklorenzotesta.youtubepreamp"

  /** Returns a new instance of [[PersistentState]] based on `localStorage` */
  def localStorage(): PersistentState = new PersistentState(LocalStorage)
}
