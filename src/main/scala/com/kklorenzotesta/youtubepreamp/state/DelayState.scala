package com.kklorenzotesta.youtubepreamp.state

/** Contains all the information about a possible state of the delay */
case class DelayState(
    time: Int = 30,
    feedback: Int = 60,
    volume: Int = 50,
    closed: Boolean = false,
)
