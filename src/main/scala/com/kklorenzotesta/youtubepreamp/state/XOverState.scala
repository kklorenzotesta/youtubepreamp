package com.kklorenzotesta.youtubepreamp.state

/** Contains all the information about a possible state of the xover */
case class XOverState(
    subLowCut: Int = 100,
    lowMidCut: Int = 250,
    midHighCut: Int = 4000,
)
