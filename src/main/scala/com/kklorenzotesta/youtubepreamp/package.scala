package com.kklorenzotesta

/** YoutubePreamp is a chrome extension that adds a panel with preamp controls to every Youtube video page. The
  * extension is organized in the following way:
  *
  * <ul>
  * <li>[[youtubepreamp.YoutubePreamp]] provides the entry point for the extension</li>
  * <li>[[youtubepreamp.audio]] contains the abstractions used for manipulate the music and generate sound effects</li>
  * <li>[[youtubepreamp.dom]] contains the abstraction used for navigate the DOM and the classes responsible for
  * the generation of the preamp panel</li>
  * <li>[[youtubepreamp.native]] contains javascript facades</li>
  * <li>[[youtubepreamp.state]] contains the abstraction used for persist some state of the preamp</li>
  * </ul>
  */
package object youtubepreamp {

  /** The full changelog of the extension, as a map from version to list of changes from the previous version */
  val Changelog: Map[String, List[String]] = Map(
    "0.1.7" -> List(
      "The Delay and Dub Siren sections can now be compacted with a double click",
      "The XOver volumes can now be reset with a double click on each volume",
      "After each update this changelog page will be displayed showing whats new",
    ),
    "0.1.6" -> List(
      "The YouTube dark mode is now applied also to the preamp",
      "Add more information in the preamp settings",
    ),
    "0.1.5" -> List(
      "Add settings to change the XOver cut points and to reset delay and dub siren",
    ),
    "0.1.4" -> List(
      "The values of the delay and the dub siren parameters are now saved instead of resetting each time",
    ),
  )
}
