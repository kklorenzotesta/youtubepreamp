package com.kklorenzotesta.youtubepreamp

import cats._
import cats.data._
import cats.implicits._
import com.kklorenzotesta.youtubepreamp.audio.DestinationStage
import com.kklorenzotesta.youtubepreamp.audio.MediaSourceStage
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.dom.DomQuery._
import com.kklorenzotesta.youtubepreamp.dom.YoutubePreampInjector
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import org.scalajs.dom.raw._

/** tarting point for the extension
  *
  * @param document      the `Document` from which information will be taken and in which the preamp will be injected
  * @param retryInterval the time after which the dom will be inspected again if the preamp injection failed, in
  *                      milliseconds
  */
private class YoutubePreamp(document: Document, retryInterval: Int = 1000) {

  import YoutubePreamp._

  /** Tries to inject the preamp in the page, if the page was not valid or not ready the injection will be
    * retried after [[retryInterval]] milliseconds.
    */
  private def injectIfAvailable(): Unit = {
    (for {
      player <- findPlayer
      related <- findInjectionLocation
    } yield {
      inject(player, related)
    }).apply(document).getOrElse(retry())
  }

  /** Injects a the preamp into the given node taking as input the given `HTMLMediaElement`.
    *
    * @param player the input for the preamp
    * @param where  the node in which the preamp panel will be injected
    */
  private def inject(player: HTMLMediaElement, where: Node): Unit = {
    val preamp = createPreamp(player)
    new YoutubePreampInjector(preamp, PersistentState.localStorage())
      .inject(where)
  }

  /** Creates a [[Preamp]] that will get his input from the given `HTMLMediaElement` and output in the tab output.
    *
    * @param player the source for the preamp input
    * @return a new Preamp already connected to the full chain
    */
  private def createPreamp(player: HTMLMediaElement): Preamp = {
    implicit val context: AudioContext = new AudioContext()
    val preamp = Preamp() ->> DestinationStage() ->> context.destination
    MediaSourceStage(player) ->> preamp
    preamp
  }

  /** Retries after [[retryInterval]] milliseconds to inject the preamp */
  private def retry(): Unit = {
    org.scalajs.dom.window
      .setTimeout(() => injectIfAvailable(), retryInterval.toDouble)
  }
}

/** The entry point for the extension */
object YoutubePreamp {

  /** First semigroup implementation allowing to fold over a list of [[DomQuery]] and return the first non empty
    * option
    *
    * @tparam A the type of the elements
    * @return a first semigroup implementation
    */
  private implicit def firstCombine[A]: Semigroup[A] = (l: A, _: A) => l

  /** Returns a [[DomQuery]] searching the main media player of the youtube page */
  private val findPlayer: DomQuery[HTMLMediaElement] =
    NonEmptyList
      .of(
        classQuery("html5-main-video", 0),
        classQuery("_mte", 0) andThen firstChild,
      )
      .map(_ andThen is[HTMLMediaElement])
      .reduce

  /** Returns a [[DomQuery]] searching the node in which the preamp can be injected as the first child */
  private val findInjectionLocation: DomQuery[Node] =
    NonEmptyList
      .of(
        id("related-skeleton") andThen parent,
        id("watch7-sidebar"),
        id("koya_child_20"),
        id("koya_child_23"),
        classQuery("ytm-autonav-bar", 0),
      )
      .reduce

  /** The main method for the extension, it detects if the injection is needed and in case performs it */
  def main(args: Array[String]): Unit =
    new YoutubePreamp(org.scalajs.dom.document).injectIfAvailable()
}
