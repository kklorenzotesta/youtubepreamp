package com.kklorenzotesta.youtubepreamp.dom.collapsible

import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.HTMLElement

/** Provides api to collapse [[elem]]. A data attribute will be add to [[elem]] in order to work properly.
  *
  * @param elem the collapsible HTMLElement
  * @param openedClass the class that will be assigned to the [[classesTarget]] when [[elem]] is open,
  *   an empty string means no class will be assigned
  * @param closedClass the class that will be assigned to the [[classesTarget]] when [[elem]] is closed,
  *   an empty string means no class will be assigned
  * @param onChanges callback invoked each time the state changes. True is passed if the new state is open
  * @param classesTarget the Element to which will be assigned the classes to mark the current state
  */
final class Collapsible private (
    val elem: HTMLElement,
    val openedClass: String,
    val closedClass: String,
    private val onChanges: Boolean => Unit,
    val classesTarget: Element,
) {
  import Collapsible._

  private var lastPadding: String = ""
  elem.style.overflow = "hidden"
  elem.setAttribute(DATA_ATTRIBUTE_NAME, "true")
  updateClasses()

  /** Opens [[elem]] if was closed */
  def open(): Unit = {
    if (!isOpen()) {
      elem.style.maxHeight = s"${elem.scrollHeight}px"
      elem.style.padding = lastPadding
      updateClasses()
      updateParents()
      onChanges(true)
    }
  }

  /** Closes [[elem]] if is open */
  def close(): Unit = {
    if (isOpen()) {
      lastPadding = elem.style.padding
      elem.style.maxHeight = "0"
      elem.style.padding = "0"
      updateClasses()
      updateParents()
      onChanges(false)
    }
  }

  /** Closes [[elem]] if is open and opens it if was closed */
  def toggle(): Unit = if (isOpen()) close() else open()

  /** Returns true if [[elem]] is currently closed */
  def isClosed(): Boolean = !isOpen()

  /** Returns true if [[elem]] is currently open */
  def isOpen(): Boolean = isOpen(elem)

  /** Returns true if [[e]] is currently open */
  private def isOpen(e: HTMLElement) = e.style.maxHeight != "0px"

  /** Gives the correct class to [[classesTarget]] based on the current [[elem]] state */
  private def updateClasses(): Unit = {
    if (isOpen()) {
      if (!openedClass.isEmpty()) classesTarget.classList.add(openedClass)
      if (!closedClass.isEmpty()) classesTarget.classList.remove(closedClass)
    } else {
      if (!openedClass.isEmpty()) classesTarget.classList.remove(openedClass)
      if (!closedClass.isEmpty()) classesTarget.classList.add(closedClass)
    }
  }

  /** Updates the collapsible parents in order to don't cut the content */
  private def updateParents(): Unit = {
    LazyList
      .iterate(Option(elem.parentElement))(opt =>
        opt.flatMap(e => Option(e.parentElement)),
      )
      .takeWhile(_.isDefined)
      .map(_.get)
      .filter(_.hasAttribute(DATA_ATTRIBUTE_NAME))
      .filter(isOpen(_))
      .foreach(e => {
        e.style.maxHeight = s"${e.scrollHeight + elem.scrollHeight}px"
      })
  }

  /** Setups an event listener that will toggle this collapsible
    *
    * @param event the event that will be listened for
    * @param of the Element on which the event will be listened
    * @param preventDefault true if preventDefault must be called on the event when triggered
    */
  def toggleOnEventOf(
      event: String,
      of: Element,
      preventDefault: Boolean = false,
  ): Collapsible = {
    of.addEventListener(
      event,
      (e: Event) => {
        this.toggle()
        if (preventDefault) e.preventDefault()
      },
    )
    this
  }
}

/** Provides factory methods for the class [[Collapsible]] */
object Collapsible {

  /** The name of the data attributed used to mark the collapsible elements in the DOM */
  private val DATA_ATTRIBUTE_NAME = "data-collapsible"

  /** Creates a [[Collapsible]]
    *
    * @param elem the element that will be able to collapse
    * @param openedClass the class that will be assigned to the classesTarget when elem is open,
    *   an empty string means no class will be assigned
    * @param closedClass the class that will be assigned to the classesTarget when elem is closed,
    *   an empty string means no class will be assigned
    * @param onChanges callback invoked each time the state changes. True is passed if the new state is open
    * @param classesTarget optionally the Element to which will be assigned the classes to mark the current state,
    *   None means that the classes will be assigned to elem
    * @return a [[Collapsible]] instance
    */
  def apply(
      elem: HTMLElement,
      openedClass: String = "open",
      closedClass: String = "close",
      onChanges: Boolean => Unit = _ => {},
      classesTarget: Option[Element] = None,
  ): Collapsible =
    new Collapsible(
      elem,
      openedClass,
      closedClass,
      onChanges,
      classesTarget.getOrElse(elem),
    )

  /** Like the default factory method but doesn't assign any classes
    *
    * @see [[Collapsible.apply]]
    *
    * @param elem the element that will be able to collapse
    * @param onChanges callback invoked each time the state changes. True is passed if the new state is open
    * @return a [[Collapsible]] instance
    */
  def noClasses(
      elem: HTMLElement,
      onChanges: Boolean => Unit = _ => {},
  ): Collapsible = new Collapsible(elem, "", "", onChanges, elem)
}
