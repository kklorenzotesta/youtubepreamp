package com.kklorenzotesta.youtubepreamp.dom

import cats.implicits._
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.dom.collapsible.Collapsible
import com.kklorenzotesta.youtubepreamp.state.DelayState
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import org.scalajs.dom.ext._
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.HTMLElement
import org.scalajs.dom.raw.HTMLInputElement
import org.scalajs.dom.raw.Node
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.section

import InjectionUtil._

/** Provides a [[DomInjector]] for the delay section of the preamp panel */
private[dom] object DelayInjector {

  /** Returns a [[DomInjector]] for the delay section of the preamp based on
    * the given Preamp and state.
    */
  def apply()(implicit preamp: Preamp, state: PersistentState): DomInjector =
    (setupOpenClose, delaySection)

  /** Generator for the full delay section of the panel */
  private def delaySection(implicit preamp: Preamp, state: PersistentState) =
    section(id := "delay")(
      switchSection("Delay", startChecked = false) { e: Event =>
        preamp.delaySwitch.setEnabled(e.checked)
      },
      section(
        sliderControl(
          "delay time",
          10,
          80,
          state.initialState.delayState.time,
        ) { e: Event =>
          preamp.delay.updateDelayTime(e.valueAsNumber / 100.0)
        }(persistDelay),
        sliderControl(
          "feedback",
          20,
          90,
          state.initialState.delayState.feedback,
        ) { e: Event =>
          preamp.delay.feedback.setVolume(e.valueAsNumber / 100.0)
        }(persistDelay),
        sliderControl(
          "delay volume",
          0,
          100,
          state.initialState.delayState.volume,
        ) { e: Event =>
          preamp.delayVolume.setVolume(e.valueAsNumber / 100.0)
        }(persistDelay),
      ),
    )

  /** Initialize the open/close of the section on the title double click */
  private def setupOpenClose(
      panel: Element,
  )(implicit state: PersistentState): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    val q: DomQuery[Collapsible] = for {
      header <- byId("delay") andThen firstChild andThen firstChild
      body <- byId("delay") andThen lastChild
    } yield {
      val collapsible =
        Collapsible(
          body.asInstanceOf[HTMLElement],
          onChanges = (isOpen) => {
            state.update(s =>
              s.copy(delayState = s.delayState.copy(closed = !isOpen)),
            )
          },
          classesTarget = Some(header),
        )
      if (state.initialState.delayState.closed) collapsible.close()
      else collapsible.open()
      collapsible.toggleOnEventOf("dblclick", header)
    }
    q(panel)
  }

  /** Persist the state of all the sliders of the delay section */
  private def persistDelay(implicit state: PersistentState) =
    (delaySliderEvent: Event) => {
      val inputs = delaySliderEvent.target
        .asInstanceOf[Node]
        .parentNode
        .parentNode
        .asInstanceOf[Element]
        .querySelectorAll("input[type=range]")
        .map(_.asInstanceOf[HTMLInputElement])
      state.update(s =>
        s.copy(
          delayState = DelayState(
            time = inputs(0).valueAsNumber.toInt,
            feedback = inputs(1).valueAsNumber.toInt,
            volume = inputs(2).valueAsNumber.toInt,
            closed = s.delayState.closed,
          ),
        ),
      )
    }

}
