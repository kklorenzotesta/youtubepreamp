package com.kklorenzotesta.youtubepreamp.dom

import cats.implicits._
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.dom.collapsible.Collapsible
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import com.kklorenzotesta.youtubepreamp.state.SirenState
import org.scalajs.dom.ext._
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.HTMLElement
import org.scalajs.dom.raw.HTMLInputElement
import org.scalajs.dom.raw.Node
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.section

import InjectionUtil._

/** Provides a [[DomInjector]] for the dub siren section of the preamp panel */
private[dom] object DubSirenInjector {

  /** Returns a [[DomInjector]] for the dub siren section of the preamp based on
    * the given Preamp and state.
    */
  def apply()(implicit preamp: Preamp, state: PersistentState): DomInjector =
    (setupOpenClose, dubSirenSection)

  /** Generator for the full dub siren section of the panel */
  private def dubSirenSection(implicit
      preamp: Preamp,
      state: PersistentState,
  ) = {
    val persist = persistSiren
    section(id := "dubsiren")(
      buttonSection("Dub Siren", "fire siren!", "whatshot") { e =>
        preamp.dubSirenSwitch.setEnabled(true)
        e.stopPropagation()
      } { _ =>
        preamp.dubSirenSwitch.setEnabled(false)
      },
      section(
        sliderControl(
          "frequency",
          160,
          1200,
          state.initialState.sirenState.frequency,
        ) { e: Event =>
          preamp.dubSiren.mainOscillator.updateFrequency(e.valueAsNumber)
        }(persist),
        sliderControl("lfo", 1, 50, state.initialState.sirenState.lfo) {
          e: Event =>
            preamp.dubSiren.updateLFO(e.valueAsNumber / 10.0)
        }(persist),
        sliderControl(
          "sine/square",
          0,
          100,
          state.initialState.sirenState.sineSquare,
        ) { e: Event =>
          preamp.dubSiren.updateSineSquareMix(e.valueAsNumber / 100.0)
        }(persist),
        sliderControl(
          "siren volume",
          0,
          100,
          state.initialState.sirenState.volume,
        ) { e: Event =>
          preamp.dubSirenVolume.setVolume(e.valueAsNumber / 100.0)
        }(persist),
      ),
    )
  }

  /** Initialize the open/close of the section on the title double click */
  private def setupOpenClose(
      panel: Element,
  )(implicit state: PersistentState): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    val q: DomQuery[Collapsible] = for {
      header <- byId("dubsiren") andThen firstChild andThen firstChild
      body <- byId("dubsiren") andThen lastChild
    } yield {
      val collapsible = Collapsible(
        body.asInstanceOf[HTMLElement],
        onChanges = (isOpen) => {
          state.update(s =>
            s.copy(sirenState = s.sirenState.copy(closed = !isOpen)),
          )
        },
        classesTarget = Some(header),
      )
      if (state.initialState.sirenState.closed) collapsible.close()
      else collapsible.open()
      collapsible.toggleOnEventOf("dblclick", header)
    }
    q(panel)
  }

  /** Persist the state of all the sliders of the siren section */
  private def persistSiren(implicit state: PersistentState) =
    (sirenSliderEvent: Event) => {
      val inputs = sirenSliderEvent.target
        .asInstanceOf[Node]
        .parentNode
        .parentNode
        .asInstanceOf[Element]
        .querySelectorAll("input[type=range]")
        .map(_.asInstanceOf[HTMLInputElement])
      state.update(s =>
        s.copy(
          sirenState = SirenState(
            frequency = inputs(0).valueAsNumber.toInt,
            lfo = inputs(1).valueAsNumber.toInt,
            sineSquare = inputs(2).valueAsNumber.toInt,
            volume = inputs(3).valueAsNumber.toInt,
            closed = s.sirenState.closed,
          ),
        ),
      )
    }
}
