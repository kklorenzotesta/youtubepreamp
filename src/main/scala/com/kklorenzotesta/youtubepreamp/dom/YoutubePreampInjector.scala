package com.kklorenzotesta.youtubepreamp.dom

import scala.scalajs.js.JSConverters._

import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.native.MaterializeFacade
import com.kklorenzotesta.youtubepreamp.native.ShadowFacade
import com.kklorenzotesta.youtubepreamp.native.chrome.Runtime
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.MutationObserver
import org.scalajs.dom.raw.MutationObserverInit
import org.scalajs.dom.raw.Node
import scalatags.JsDom.all._

/** Allows the creation of preamp panels in the DOM controlling the given
  * [[com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp]]
  *
  * @param preamp the preamp that will be controlled by the created panels
  * @param state the persistent state of the preamp
  */
class YoutubePreampInjector(preamp: Preamp, state: PersistentState) {

  import YoutubePreampInjector._

  /** Creates a new preamp panel and injects it into the given `Node` */
  def inject(into: Node): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => _, _}
    val iconsLink = link(href := materialIconSource, rel := "stylesheet")
    tagQuery("head", 0)(into.ownerDocument).foreach(iconsLink.inject(_))
    val wrapper = div(
      id := "preamp-wrapper",
      cls := "action-panel-content yt-card yt-card-has-padding",
    ).render
    into.insertBefore(wrapper, into.firstChild)
    val shadowRoot = ShadowFacade.executeAttachShadow(wrapper, "open")
    iconsLink.inject(shadowRoot)
    link(href := Runtime.getURL("css/nouislider.css"), rel := "stylesheet")
      .inject(shadowRoot)
    link(href := Runtime.getURL("css/preamp.css"), rel := "stylesheet")
      .inject(shadowRoot)
    PreampInjector()(preamp, state).inject(shadowRoot)
    MaterializeFacade.AutoInit(shadowRoot)
    def updateDarkMode(): Unit = {
      if (into.ownerDocument.documentElement.hasAttribute("dark")) {
        wrapper.asInstanceOf[Element].setAttribute("dark", "true")
      } else {
        wrapper.asInstanceOf[Element].removeAttribute("dark")
      }
    }
    val darkModeObserver = new MutationObserver((_, _) => {
      updateDarkMode()
    })
    darkModeObserver.observe(
      into.ownerDocument.documentElement,
      MutationObserverInit(
        attributes = true,
        attributeFilter = Array("dark").toJSArray,
      ),
    )
    updateDarkMode()
  }
}

/** Provides constants for the [[YoutubePreampInjector]]. */
private object YoutubePreampInjector {

  /** The url for the material icons font */
  private val materialIconSource =
    "https://fonts.googleapis.com/icon?family=Material+Icons"
}
