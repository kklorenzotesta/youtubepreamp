package com.kklorenzotesta.youtubepreamp.dom

import cats._
import cats.data._
import cats.implicits._
import org.scalajs.dom.ext._
import org.scalajs.dom.raw._

/** Implicit conversions and helpers for the type [[DomQuery]] */
object DomQuery {

  /** A DomQuery is a function extracting something from the dom, either from a `Document` or from an `Element`
    * The function will return a [[scala.Left]] in case of failure.
    *
    * @tparam A the type of the extraction value
    */
  type DomQuery[A] = Kleisli[Either[String, *], Either[Document, Element], A]

  /** Compile type proof that the all the `Element`s with tag in `tags` are instance of the type `T`
    *
    * @param tags the tags with type `T`
    * @tparam T the common type of the `tags`
    */
  class TagClass[T] private[DomQuery] (val tags: List[String])

  /** Proof that 'a' tags are `HTMLAnchorElements` */
  implicit val anchorTagClass: TagClass[HTMLAnchorElement] = new TagClass(
    List("a"),
  )

  implicit val inputTagClass: TagClass[HTMLInputElement] = new TagClass(
    List("input"),
  )

  /** Proof that 'video' or 'audio' tags are `HTMLMediaElements` */
  implicit val mediaElementTagClass: TagClass[HTMLMediaElement] = new TagClass(
    List("video", "audio"),
  )

  /** Provides extension functions for [[cats.data.Kleisli]] */
  implicit class KleisliOps[F[_], A, B](k: Kleisli[F, A, B]) {

    /** Lifts a Kleisli[F, A, B] to a Kleisli[F, G[A], G[B]] */
    def liftF[G[_]: Traverse](implicit
        applicativeF: Applicative[F],
    ): Kleisli[F, G[A], G[B]] =
      Kleisli(Functor[G].lift(k.run).map(_.sequence))
  }

  /** Creates a [[DomQuery]] combining the two given functions returning Eithers.
    *
    * @param l the extractor from a `Document`
    * @param r the extraction from an `Element`
    * @tparam A the type of the extracted value
    * @return the DomQuery obtained by combining the functions
    */
  def apply[A](
      l: Document => Either[String, A],
      r: Element => Either[String, A],
  ): DomQuery[A] =
    from(_.fold(l(_), r(_)))

  /** Creates a [[DomQuery]] combining the two given functions returning Options.
    *
    * @param l the extractor from a `Document`
    * @param r the extraction from an `Element`
    * @param left the String that will be boxed in a [[scala.Left]] in case
    *             the applied function returns [[scala.None]]
    * @tparam A the type of the extracted value
    * @return the DomQuery obtained by combining the functions
    */
  def apply[A](
      l: Document => Option[A],
      r: Element => Option[A],
  )(left: Node => String): DomQuery[A] =
    from(_.fold(d => l(d).toRight(left(d)), e => r(e).toRight(left(e))))

  /** Since Either is covariant provides automatic widening of [[DomQuery]]s
    *
    * @param query the query which type will be widen
    * @tparam A the type that will be widen
    * @tparam B the super type of `A` which will be produced by the new DomQuery
    * @return a new DomQuery with a less specific type
    */
  implicit def widen[A, B >: A](query: DomQuery[A]): DomQuery[B] = query.widen

  /** Converts the function to its corresponding [[DomQuery]]
    *
    * @param f the function that will be converted
    * @tparam A the type of the value extracted from the dom
    * @return the equivalent DomQuery
    */
  implicit def from[A](
      f: Either[Document, Element] => Either[String, A],
  ): DomQuery[A] =
    Kleisli(f)

  /** Since `Node` is a common super type of both `Document` and `Element`, if a value can be extracted from a
    * Node by a given function, then a [[DomQuery]] can be constructed using that function.
    *
    * @param f the function extraction a value from Node
    * @tparam A the type of the value extracted
    * @return the corresponding DomQuery
    */
  implicit def fromNode[A](f: Node => Either[String, A]): DomQuery[A] =
    DomQuery(f(_), f(_))

  /** Creates a [[DomQuery]] that will only succeed when a `Document` is provided using the given function, and return
    * Left with a generic error message when an `Element` is provided.
    *
    * @param l the function that will be applied to the Documents
    * @tparam A the type of the value extracted
    * @return a DomQuery working only on Documents
    */
  implicit def fromL[A](l: Document => A): DomQuery[A] =
    fromLE(l andThen (Right(_)))

  /** Creates a [[DomQuery]] that will only succeed when a `Element` is provided using the given function, and return
    * Left with a generic error message when an `Document` is provided.
    *
    * @param r the function that will be applied to the Elements
    * @tparam A the type of the value extracted
    * @return a DomQuery working only on Elements
    */
  implicit def fromR[A](r: Element => A): DomQuery[A] =
    fromRE(r andThen (Right(_)))

  /** Creates a [[DomQuery]] that will only succeed when a `Document` is provided and the given function succeed,
    * and return Left with a generic error message when an `Element` is provided.
    *
    * @param l the function that will be applied to the Documents
    * @tparam A the type of the value extracted
    * @return a DomQuery working only on Documents
    */
  implicit def fromLE[A](l: Document => Either[String, A]): DomQuery[A] =
    DomQuery(l, _ => Left("Element is not supported from this query"))

  /** Creates a [[DomQuery]] that will only succeed when a `Element` is provided and the given function succeed,
    * and return Left with a generic error message when an `Document` is provided.
    *
    * @param r the function that will be applied to the Elements
    * @tparam A the type of the value extracted
    * @return a DomQuery working only on Elements
    */
  implicit def fromRE[A](r: Element => Either[String, A]): DomQuery[A] =
    DomQuery(_ => Left("Document is not supported from this query"), r)

  /** Specialize the given [[DomQuery]] to only accept `Document`s, allowing direct invocation by passing a Document
    *
    * @param query the query that will be specialized
    * @tparam A the type of the value extracted
    * @return a [[cats.data.Kleisli]] that accepts Documents
    */
  implicit def specializeL[A](
      query: DomQuery[A],
  ): Kleisli[Either[String, *], Document, A] =
    query.local(Left(_))

  /** Specialize the given [[DomQuery]] to only accept `Element`s, allowing direct invocation by passing an Element
    *
    * @param query the query that will be specialized
    * @tparam A the type of the value extracted
    * @return a [[cats.data.Kleisli]] that accepts Elements
    */
  implicit def specializeR[A](
      query: DomQuery[A],
  ): Kleisli[Either[String, *], Element, A] =
    query.local(Right(_))

  /** Returns a [[DomQuery]] which accepts and converts all the element that are of type `T`,
    * to avoid the use of the reflection a [[TagClass]] is required.
    *
    * @tparam T the type of elements that are accepted
    * @return a [[DomQuery]] performing the check and cast for the Elements of type T
    */
  def is[T <: Element: TagClass]: DomQuery[T] = (element: Element) => {
    val tags = implicitly[TagClass[T]].tags.map(_.toLowerCase)
    if (tags.contains(element.tagName.toLowerCase)) {
      Right(element.asInstanceOf[T])
    } else {
      Left(s"$element is not a tag of {${tags.mkString(",")}}")
    }
  }

  /** Returns a [[DomQuery]] that search all the elements of class `className`
    *
    * @param className the name of the searched class
    * @return a [[DomQuery]] searching the nth element of class className
    */
  def classQuery(className: String): DomQuery[List[Element]] =
    from(either => {
      Option(
        either.fold(
          _.getElementsByClassName(className),
          _.getElementsByClassName(className),
        ),
      ).flatMap(c => Option(c.toList))
        .toRight(
          throw new AssertionError(
            "unexpected null from getElementsByClassName",
          ),
        )
    })

  /** Returns a [[DomQuery]] that search the `nth` element of class `className`
    *
    * @param className the name of the searched class
    * @param nth       the index of the element searched with that class
    * @return a [[DomQuery]] searching the nth element of class className
    */
  def classQuery(className: String, nth: Int): DomQuery[Element] =
    classQuery(className) andThen (l =>
      l.get(nth.toLong)
        .toRight(
          s"$nth element of classlist for $className doesn't exists, results: $l",
        ),
    )

  /** Returns a [[DomQuery]] that search all the elements with the tag `tagName`
    *
    * @param tagName the name of the searched tag
    * @return a [[DomQuery]] searching the nth element of tag tagName
    */
  def tagQuery(tagName: String): DomQuery[List[Element]] =
    from(either => {
      Option(
        either.fold(
          _.getElementsByTagName(tagName),
          _.getElementsByTagName(tagName),
        ),
      ).flatMap(c => Option(c.toList))
        .toRight(
          throw new AssertionError(
            "unexpected null from getElementsByTagName",
          ),
        )
    })

  /** Returns a [[DomQuery]] that search the `nth` element with the tag `tagName`
    *
    * @param tagName the name of the searched tag
    * @param nth     the index of the element searched with that tag
    * @return a [[DomQuery]] searching the nth element of tag tagName
    */
  def tagQuery(tagName: String, nth: Int): DomQuery[Element] =
    tagQuery(tagName) andThen (l =>
      l.get(nth.toLong)
        .toRight(
          s"$nth element of taglist for $tagName doesn't exists, results: $l",
        ),
    )

  /** A [[DomQuery]] searching the first child if exists */
  val firstChild: DomQuery[Element] = DomQuery(
    d => Option(d.firstElementChild),
    e => Option(e.firstElementChild),
  )(n => s"a first child for $n doesn't exists")

  /** A [[DomQuery]] searching the last child if exists */
  val lastChild: DomQuery[Element] = DomQuery(
    d => Option(d.lastElementChild),
    e => Option(e.lastElementChild),
  )(n => s"a last child for $n doesn't exists")

  /** A [[DomQuery]] searching the parent Node if exists */
  val parent: DomQuery[Node] = (n: Node) =>
    Option(n.parentNode).toRight(s"$n doesn't have any parent")

  /** A [[DomQuery]] that returns the input Element and fails in case of Document */
  val queryRoot: DomQuery[Element] = fromR(identity)

  /** Returns a [[DomQuery]] that search the `Element` with the given id if exists
    *
    * @param id the searched id
    * @return a [[DomQuery]] searching the Element with the given id
    */
  def id(id: String): DomQuery[Element] =
    DomQuery(
      d => Option(d.getElementById(id)),
      e => Option(e.querySelector(s"#$id")),
    )(n => s"can't find an Element with id $id from $n")
}
