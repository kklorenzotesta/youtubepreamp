package com.kklorenzotesta.youtubepreamp.dom

import scala.scalajs.js.JSConverters._

import cats.implicits._
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.audio.preamp.XOver.XOverBand
import com.kklorenzotesta.youtubepreamp.audio.preamp.XOver.XOverBand._
import com.kklorenzotesta.youtubepreamp.dom.collapsible.Collapsible
import com.kklorenzotesta.youtubepreamp.native.NoUISliderFacade
import org.scalajs.dom.ext._
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.HTMLElement
import org.scalajs.dom.raw.HTMLInputElement
import scalatags.JsDom
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.section

import InjectionUtil._

/** Provides a [[DomInjector]] for the xover section of the preamp panel */
private[dom] object XOverInjector {

  /** Returns a [[DomInjector]] for the xover section of the preamp based on
    * the given Preamp.
    */
  def apply()(implicit preamp: Preamp): DomInjector =
    (initializeSliders |+| setupOpenClose, xOverSection)

  private def initializeSliders(implicit preamp: Preamp) = (panel: Element) => {
    bands.foreach { band =>
      val slider = panel.querySelector("#" + bandVolumeId(band))
      val noUiSlider =
        NoUISliderFacade.createVerticalSlider(
          slider,
          0,
          200,
          100,
          newValue => {
            preamp.xOver.getVolume(band).setVolume(newValue / 100.0)
          },
        )
      slider.addEventListener(
        "dblclick",
        (_: Event) => {
          noUiSlider.set(Array(100.0).toJSArray)
        },
      )
    }
  }

  /** Generator for the volumes section of the crossover section of the panel */
  private val xOverVolumeSection = section(cls := "xover-volumes")(
    header(cls := "preamp-show-volumes")(
      i(cls := "material-icons center-aling")("keyboard_arrow_down"),
    ),
    section(
      ul(
        for (idV <- XOverBand.bands.map(bandVolumeId))
          yield li(
            div(id := idV),
          ),
      ),
      div(cls := "center-align")("X-over volumes"),
    ),
  )

  /** Generator for the switch section of the crossover section of the panel */
  private def xOverSwitchesSection(implicit preamp: Preamp) = ul()(
    for (band <- XOverBand.bands)
      yield li(cls := "switch")(
        h5(
          bandLabelAction(band)
            .map(f =>
              a(
                href := "#",
                onclick := { e: Event =>
                  e.preventDefault()
                  f(e)
                },
              )(bandLabel(band)),
            )
            .getOrElse(bandLabel(band)),
        ),
        label(
          input(
            cls := "red accent-4",
            `type` := "checkbox",
            checked,
            onchange := { e: Event =>
              preamp.xOver.getDisconnectableBand(band).setEnabled(e.checked)
            },
          ),
          span(cls := "lever vertical"),
        ),
      ),
  )

  /** Generator for the full crossover section of the panel */
  def xOverSection(implicit preamp: Preamp): JsDom.TypedTag[Element] =
    section(id := "xover")(
      switchSection("X-Over", idName = "xover-switch-section") { e: Event =>
        preamp.xOverSwitch.setOnMain(e.checked)
      },
      xOverSwitchesSection,
      xOverVolumeSection,
    )

  /** Initialize the open/close of the volumes section */
  private def setupOpenClose(
      panel: Element,
  ): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    val findVolumeSection = byId("xover") andThen lastChild
    val q: DomQuery[Collapsible] = for {
      volumesSection <- findVolumeSection
      header <- findVolumeSection andThen firstChild andThen firstChild
      body <- findVolumeSection andThen lastChild
    } yield {
      val collapsible =
        Collapsible(
          body.asInstanceOf[HTMLElement],
          classesTarget = Some(volumesSection),
        )
      collapsible.close()
      collapsible.toggleOnEventOf("click", header)
    }
    q(panel)
  }

  /** Returns optionally the action to be performed after a click on the label of the given [[XOverBand]] */
  private def bandLabelAction(band: XOverBand): Option[Event => Unit] =
    band match {
      case Sub =>
        Some { e: Event =>
          val ul = e.parentElement.parentElement.parentElement
          val inputs = ul
            .getElementsByTagName("input")
            .map(_.asInstanceOf[HTMLInputElement])
            .toList
          if (inputs.head.checked) {
            if (
              inputs.tail
                .map(_.checked)
                .forall(_ == inputs.tail.head.checked)
            ) {
              if (inputs.tail.head.checked) {
                inputs.tail.filter(_.checked).foreach(_.click())
              } else {
                inputs.tail.filter(!_.checked).foreach(_.click())
              }
            } else {
              inputs.tail.filter(_.checked).foreach(_.click())
            }
          } else {
            inputs.head.click()
            inputs.tail.filter(_.checked).foreach(_.click())
          }
        }
      case Low =>
        Some { e: Event =>
          val ul = e.parentElement.parentElement.parentElement
          val inputs = ul
            .getElementsByTagName("input")
            .map(_.asInstanceOf[HTMLInputElement])
            .toList
            .take(2)
          if (inputs.forall(_.checked == false)) {
            inputs.foreach(_.click())
          } else {
            inputs.filter(_.checked).foreach(_.click())
          }
        }
      case _ => None
    }

  /** Returns the id of the volume slider wrapper for the given [[XOverBand]] */
  private def bandVolumeId(band: XOverBand): String =
    "xover-band-volume-" + bandLabel(band)
}
