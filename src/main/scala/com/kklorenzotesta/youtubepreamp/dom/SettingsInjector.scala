package com.kklorenzotesta.youtubepreamp.dom

import scala.scalajs.js.JSConverters._

import cats.data._
import cats.implicits._
import com.kklorenzotesta.youtubepreamp.BuildInfo
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.audio.preamp.XOver.XOverBand
import com.kklorenzotesta.youtubepreamp.native.EventFacade
import com.kklorenzotesta.youtubepreamp.native.NoUISliderFacade
import com.kklorenzotesta.youtubepreamp.state.DelayState
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import com.kklorenzotesta.youtubepreamp.state.SirenState
import com.kklorenzotesta.youtubepreamp.state.XOverState
import org.scalajs.dom.ext._
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.HTMLInputElement
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.article
import scalatags.JsDom.tags2.section
import shapeless.Generic

import InjectionUtil._

/** Provides a [[DomInjector]] for the delay section of the preamp panel */
private[dom] object SettingsInjector {

  /** Returns a [[DomInjector]] for the settings modal section of the preamp based on
    * the given Preamp and state.
    */
  def apply()(implicit preamp: Preamp, state: PersistentState): DomInjector =
    (
      initializeSlider |+| initializeResetSirenButton |+|
        initializeResetDelayButton,
      settings,
    )

  /** Generator for the settings modal */
  private def settings(implicit preamp: Preamp) =
    article(id := "preamp-settings", cls := "modal")(
      section(cls := "modal-content")(
        h4("YoutubePreamp Settings"),
        section(id := "crossover-settings")(
          h5("Crossover frequencies:"),
          div(id := "xover-setting-slider"),
          ul(
            for (cut <- xOverCuts(preamp))
              yield li(
                p(cut._1),
                input(
                  `type` := "numeric",
                  cls := "xover-setting-cut-input",
                  size := 5,
                  disabled,
                ),
              ),
            li(
              button(
                cls := "btn",
                id := "preamp-settings-reset-xover-button",
              )("Reset"),
            ),
          ),
        ),
        section(id := "reset-settings")(
          h5("Reset settings:"),
          button(
            cls := "btn",
            id := "preamp-settings-reset-delay-button",
          )("Reset delay"),
          button(
            cls := "btn",
            id := "preamp-settings-reset-siren-button",
          )("Reset siren"),
        ),
        footer(
          p(s"Version: ${BuildInfo.version}"),
          a(
            href := "https://gitlab.com/kklorenzotesta/youtubepreamp",
            target := "_blank",
          )(
            "Contribute on",
            img(
              src := "https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-rgb.png",
            ),
          ),
        ),
      ),
    )

  /** Returns a List with the labels and the callback functions to be invoked on update
    * for each xover cut
    */
  private def xOverCuts(preamp: Preamp): List[(String, Int => Unit)] =
    XOverBand.bands
      .sliding(2, 1)
      .collect { case lower :: upper :: _ =>
        (
          bandLabel(lower) + "/" + bandLabel(upper) + ":",
          (newCut: Int) => {
            preamp.xOver.getBand(lower).setUpperBound(newCut)
            preamp.xOver.getBand(upper).setLowerBound(newCut)
          },
        )
      }
      .toList

  /** Initializes the reset button for the delay */
  private def initializeResetDelayButton: Element => Unit =
    initializeResetSlider(
      "preamp-settings-reset-delay-button",
      "delay",
      Generic[DelayState].to(DelayState()).init.toList,
    )

  /** Initializes the reset button for the dub siren */
  private def initializeResetSirenButton: Element => Unit =
    initializeResetSlider(
      "preamp-settings-reset-siren-button",
      "dubsiren",
      Generic[SirenState].to(SirenState()).init.toList,
    )

  /** Initializes a reset button */
  private def initializeResetSlider(
      resetButtonId: String,
      slidersSectionId: String,
      defaultValues: List[Int],
  ): Element => Unit = {
    import DomQuery._
    (for {
      resetButton <- id(resetButtonId)
      sliders <- id(slidersSectionId) andThen tagQuery("input") andThen
        specializeR(is[HTMLInputElement]).liftF[List]
    } yield {
      resetButton.addEventListener(
        "click",
        { _: Event =>
          val zipValues = ZipList(defaultValues)
          val fs =
            ZipList(
              sliders
                .filter(_.`type` == "range")
                .map(i => {
                  v: Int => {
                    i.value = v.toString()
                    i.dispatchEvent(EventFacade.createEvent("input"))
                    i.dispatchEvent(EventFacade.createEvent("mouseup"))
                  }
                }),
            )
          fs <*> zipValues
        },
      )
    })
  }

  /** Initializes the vertical slides in the given panel with the nouislider APIs */
  private def initializeSlider(implicit
      preamp: Preamp,
      state: PersistentState,
  ): Element => Unit = {
    import DomQuery._
    (for {
      xOverSettingSlider <- id("xover-setting-slider")
      resetButton <- id("preamp-settings-reset-xover-button")
      inputs <- classQuery("xover-setting-cut-input") andThen
        specializeR(is[HTMLInputElement]).liftF[List]
    } yield {
      val noUiSlider = NoUISliderFacade.createXOverSettingsSlider(
        xOverSettingSlider,
        Generic[XOverState].to(state.initialState.xoverState).toList.toJSArray,
        values => {
          state.update(
            _.copy(xoverState = XOverState(values(0), values(1), values(2))),
          )
          val zipValues = ZipList(values.toList)
          ZipList(xOverCuts(preamp).map(_._2)) <*> zipValues
          ZipList(
            inputs
              .map(node => {
                value: Int => node.value = value.toString()
              })
              .toList,
          ) <*> zipValues
        },
      )
      resetButton.addEventListener(
        "click",
        { _: Event =>
          noUiSlider.set(
            Generic[XOverState]
              .to(XOverState())
              .toList
              .map(_.toDouble)
              .toJSArray,
          )
        },
      )
    })
  }
}
