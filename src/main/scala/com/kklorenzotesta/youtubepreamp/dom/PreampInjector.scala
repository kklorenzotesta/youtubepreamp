package com.kklorenzotesta.youtubepreamp.dom

import cats.implicits._
import com.kklorenzotesta.youtubepreamp.BuildInfo
import com.kklorenzotesta.youtubepreamp.audio.preamp.Preamp
import com.kklorenzotesta.youtubepreamp.dom.collapsible.Collapsible
import com.kklorenzotesta.youtubepreamp.native.EventFacade
import com.kklorenzotesta.youtubepreamp.native.chrome.Runtime
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import org.scalajs.dom.ext._
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.HTMLElement
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.article

/** Provides a [[DomInjector]] for the full preamp panel */
private[dom] object PreampInjector {

  /** Returns a [[DomInjector]] for the full preamp panel based on
    * the given Preamp and state.
    */
  def apply()(implicit preamp: Preamp, state: PersistentState): DomInjector =
    preampPanel.swap
      .map(_ |+| alignState |+| setupOpenClose |+| openChangelogIfNewVersion)
      .swap

  /** Generator for the [[DomInjector]] of the full preamp panel without the align state inizialization */
  private def preampPanel(implicit
      preamp: Preamp,
      state: PersistentState,
  ): DomInjector =
    for {
      xOverSection <- XOverInjector()
      delaySection <- DelayInjector()
      dubSirenSection <- DubSirenInjector()
      menu <- MenuInjector()
      settings <- SettingsInjector()
    } yield article(id := "preamp-panel")(
      mainHeader,
      form(id := "preamp-body")(
        xOverSection,
        delaySection,
        dubSirenSection,
      ),
      menu,
      settings,
    )

  /** Generator for the main header of the panel */
  private val mainHeader = header(cls := "valign-wrapper")(
    img(src := Runtime.getURL("icons/icon-128.png"), cls := "responsive-img"),
    h5("Youtube Preamp"),
    i(
      cls := "dropdown-trigger material-icons center-align",
      id := "preamp-menu-trigger",
      attr("data-target") := "preamp-menu",
    )("more_vert"),
  )

  /** Updates the [[Preamp]] state with the currently displayed values */
  private def alignState(panel: Element): Unit = {
    panel
      .getElementsByTagName("input")
      .foreach(_.dispatchEvent(EventFacade.createEvent("input")))
    panel
      .getElementsByTagName("input")
      .foreach(_.dispatchEvent(EventFacade.createEvent("change")))
  }

  /** Initialize the open/close of the panel on the header double click */
  private def setupOpenClose(
      panel: Element,
  )(implicit state: PersistentState): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    val q: DomQuery[Collapsible] = for {
      header <- firstChild
      menuButton <- byId("preamp-menu") andThen firstChild andThen firstChild
      collapsible <- byId("preamp-body").map(e =>
        Collapsible(
          e.asInstanceOf[HTMLElement],
          onChanges = (isOpen) => {
            menuButton.innerText = if (isOpen) "Close" else "Open"
            state.update(_.copy(closed = !isOpen))
          },
          classesTarget = Some(header),
        ),
      )
    } yield {
      if (state.initialState.closed) collapsible.close() else collapsible.open()
      collapsible
        .toggleOnEventOf("dblclick", header)
        .toggleOnEventOf("click", menuButton, true)
    }
    q(panel)
  }

  /** Opens the changelog if the version is changed from the last start */
  private def openChangelogIfNewVersion(
      panel: Element,
  )(implicit state: PersistentState): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    if (state.read.lastVersion != BuildInfo.version) {
      byId("open-changelog-trigger")(panel).foreach(
        _.dispatchEvent(EventFacade.createEvent("click")),
      )
    }
  }
}
