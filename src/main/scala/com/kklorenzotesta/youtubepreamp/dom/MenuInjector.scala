package com.kklorenzotesta.youtubepreamp.dom

import com.kklorenzotesta.youtubepreamp.BuildInfo
import com.kklorenzotesta.youtubepreamp.native.EventFacade
import com.kklorenzotesta.youtubepreamp.state.PersistentState
import org.scalajs.dom.ext._
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.Event
import scalatags.JsDom.all._

/** Provides a [[DomInjector]] for the dropdown section of the preamp panel */
private[dom] object MenuInjector {

  /** Returns a [[DomInjector]] for the dub siren section of the preamp based on
    * the given state.
    */
  def apply()(implicit state: PersistentState): DomInjector =
    (setupChangelogOpen, menu)

  /** Generator for the main dropdown menu of the panel */
  private def menu(implicit state: PersistentState) =
    ul(id := "preamp-menu", cls := "dropdown-content")(
      li(
        a(href := "#!")(if (state.initialState.closed) "Open" else "Close"),
      ),
      li(
        cls := "divider",
        tabindex := -1,
      ),
      li(
        a(
          href := "#preamp-settings",
          cls := "modal-trigger",
        )("Settings"),
      ),
      li(
        cls := "divider",
        tabindex := -1,
      ),
      li(
        a(href := "#!", id := "open-changelog-trigger")("Changelog"),
      ),
    )

  /** Initialize the open changelog button in the menu */
  private def setupChangelogOpen(
      panel: Element,
  )(implicit state: PersistentState): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    byId("open-changelog-trigger")(panel).foreach(
      _.addEventListener(
        "click",
        { e: Event =>
          e.preventDefault()
          if (state.read.closed == true) {
            val q: DomQuery[Element] =
              byId("preamp-menu") andThen firstChild andThen firstChild
            q(panel).foreach(_.dispatchEvent(EventFacade.createEvent("click")))
          }
          ChangelogInjector().inject(panel)
          state.update(_.copy(lastVersion = BuildInfo.version))
        },
      ),
    )
  }
}
