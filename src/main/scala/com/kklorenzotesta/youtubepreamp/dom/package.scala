package com.kklorenzotesta.youtubepreamp
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.Node
import scalatags.JsDom

/** Contains all the classes related to the DOM manipulation. In particular [[dom.DomQuery]] allows to navigate the
  * DOM to extract some value in a pure and type safe way, [[dom.DomInjector]] allows create some Element and
  * execute a function after each time is created and [[dom.YoutubePreampInjector]] is responsible for the
  * creation and injection of the preamp panel.
  */
package object dom {

  /** A dom injector is a scalatags TypedTag ready to be rendered and a callback function to be invoked each time
    * the tag is redered with the resulting Element
    */
  type DomInjector = (Element => Unit, JsDom.TypedTag[Element])

  /** Creates a [[DomInjector]] from the tag with a callback function that does nothing */
  implicit def injectorFromTag(tag: JsDom.TypedTag[Element]): DomInjector =
    (_ => (), tag)

  /** Provides extension functions to render a scalatags tag */
  implicit class TagOps(val tag: JsDom.TypedTag[Element]) {

    /** Injects into the given node the tag */
    def inject(into: Node): Unit = injectorFromTag(tag).inject(into)
  }

  /** Provides extension functions to inject a [[DomInjector]] */
  implicit class DomInjectorOps(val injector: DomInjector) {

    /** Injects into the given node the tag of the [[DomInjector]] and calls its callback */
    def inject(into: Node): Unit = {
      val element = injector._2.render
      into.appendChild(element)
      injector._1(element)
    }
  }
}
