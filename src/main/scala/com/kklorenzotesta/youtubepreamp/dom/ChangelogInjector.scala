package com.kklorenzotesta.youtubepreamp.dom

import com.kklorenzotesta.youtubepreamp.Changelog
import com.kklorenzotesta.youtubepreamp.native.chrome.Runtime
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.Event
import scalatags.JsDom.all._
import scalatags.JsDom.tags2.section

/** Provides a [[DomInjector]] for the changelog overlay of the preamp panel */
private[dom] object ChangelogInjector {

  /** Returns a [[DomInjector]] for the changelog overlay */
  def apply(): DomInjector = (setupClose, changelogSection)

  /** Generator for the changelog overlay */
  private def changelogSection = section(id := "changelog")(
    header(cls := "valign-wrapper")(
      img(src := Runtime.getURL("icons/icon-128.png"), cls := "responsive-img"),
      h5("Changelog"),
      i(cls := "material-icons center-align", id := "close-changelog-trigger")(
        "close",
      ),
    ),
    ul(id := "changelog-list")(Changelog.toList.map { case (version, changes) =>
      li(
        h5(s"New in version $version"),
        ul(
          changes.map(change => li(p(change))),
        ),
      )
    }),
  )

  /** Initialize the close changelog button on the changelog header */
  private def setupClose(changelogSection: Element): Unit = {
    import com.kklorenzotesta.youtubepreamp.dom.DomQuery.{id => byId, _}
    val q: DomQuery[Unit] = for {
      closeButton <- byId("close-changelog-trigger")
      list <- byId("changelog-list")
    } yield {
      List(closeButton, list).foreach(
        _.addEventListener(
          "click",
          { _: Event =>
            changelogSection.parentNode.removeChild(changelogSection)
          },
        ),
      )
    }
    q(changelogSection)
  }
}
