package com.kklorenzotesta.youtubepreamp.native

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

import org.scalajs.dom.raw.Event

/** Javascript facade for allow to manipulate `Event`s */
@js.native
@JSGlobalScope
object EventFacade extends js.Object {

  /** Creates an `Event` with the given name
    *
    * @param name the name for the event
    * @return the created `Event`
    */
  def createEvent(name: String): Event = js.native
}
