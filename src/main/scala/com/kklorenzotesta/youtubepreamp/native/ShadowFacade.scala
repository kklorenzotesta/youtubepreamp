package com.kklorenzotesta.youtubepreamp.native

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

import org.scalajs.dom.raw.Element

/** Javascript facade for shadow DOM APIs */
@js.native
@JSGlobalScope
object ShadowFacade extends js.Object {

  /** Attaches a shadow DOM tree to the specified element and returns a reference to its ShadowRoot
    *
    * @param element the element to which the shadow DOM tree will be attached
    * @param mode    a string specifying the encapsulation mode for the shadow DOM tree
    * @return a reference to its ShadowRoot
    */
  def executeAttachShadow(element: Element, mode: String): Element = js.native
}
