package com.kklorenzotesta.youtubepreamp.native.chrome

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

/** This module provides information about your extension and the environment it's running in. */
@JSGlobal("chrome.runtime")
@js.native
object Runtime extends js.Object {

  /** Given a relative path from the manifest.json to a resource packaged with the extension, return a fully-qualified URL. */
  def getURL(path: String): String = js.native
}
