package com.kklorenzotesta.youtubepreamp.native.chrome

/** Contains facade for the Chrome API */
package object chrome {}
