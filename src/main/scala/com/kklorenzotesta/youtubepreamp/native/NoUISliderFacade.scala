package com.kklorenzotesta.youtubepreamp.native

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

import org.scalajs.dom.raw.Node

/** Javascript facade for nouislider APIs */
@js.native
@JSGlobalScope
object NoUISliderFacade extends js.Object {

  /** Creates a vertical slider with the given configuration
    *
    * @param slider     the `Node` that will be transformed into a slider
    * @param minValue   the minimum value of the slider
    * @param maxValue   the maximum value of the slider
    * @param startValue the starting value of the slider
    * @param onUpdate   the function that will be invoked when the value is updated
    */
  def createVerticalSlider(
      slider: Node,
      minValue: Int,
      maxValue: Int,
      startValue: Int,
      onUpdate: js.Function1[Int, Unit],
  ): NoUiSlider = js.native

  /** Creates the slider for setting the xover cuts
    *
    * @param slider         the `Node` that will be transformed into the slider
    * @param startingValues the starting values for the cut points
    * @param onUpdate       the function that will be invoked when a value is updated
    */
  def createXOverSettingsSlider(
      slider: Node,
      startingValues: js.Array[Int],
      onUpdate: js.Function1[js.Array[Int], Unit],
  ): NoUiSlider = js.native
}
