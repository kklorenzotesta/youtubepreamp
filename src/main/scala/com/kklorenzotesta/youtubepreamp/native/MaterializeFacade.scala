package com.kklorenzotesta.youtubepreamp.native

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

import org.scalajs.dom.raw.Element

/** Javascript facade for access Materialize APIs */
@js.native
@JSGlobal("M")
object MaterializeFacade extends js.Object {

  /** Auto Init allows you to initialize all of the Materialize Components with a single function call.
    * It is important to note that you cannot pass in options using this method.
    *
    * @param context DOM Element to search within for components
    */
  def AutoInit(context: Element): Unit = js.native
}
