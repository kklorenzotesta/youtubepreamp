package com.kklorenzotesta.youtubepreamp.native

import scala.scalajs.js

/** Javascript facade for a NoUiSlider instance */
@js.native
trait NoUiSlider extends js.Object {

  /** Sets the given values for the slider */
  def set(values: js.Array[Double]): Unit
}
